//Projet Atlantide
//    Copyright (C) 2016  Raphaël Charrondière
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#include "contentformatter.h"
#include <QString>
#include <QMainWindow>
#include <QRegularExpression>
#include <Miniscritpt.h>

namespace StoriesAreMyHistory {

void ContentFormatter::FormatFromCommand(std::string &content, ParserCommand &command, ParserException ifNeeded)
{
    if(command.GetName()=="Titre")
    {
        closeParagraphIfNeeded(content);
        SurroundNonParagraphContent(content,command,ifNeeded,"<h1>", "</h1>");
    }
    else if(command.GetName()=="SousTitre")
    {
        closeParagraphIfNeeded(content);
        SurroundNonParagraphContent(content,command,ifNeeded,"<h2>", "</h2>");
    }
    else if(command.GetName()=="Centré")
    {
        closeParagraphIfNeeded(content);
        SurroundContent(content,command,ifNeeded,"<center>", "</center>");
    }
    else if(command.GetName()=="Donnée")
    {
        CheckCommandArgumentNumber(command,ifNeeded);
        if(!command.getArgumentRefAt(0).isALitteralString())
        {
            ifNeeded.message="Donnée command should have a variable as argument";
            throw ifNeeded;
        }
        content+="<&>"+command.getArgumentRefAt(0).getLitteralRef()+"<:><&>";
    }
    else if(command.GetName()=="Image")
    {
        CheckCommandArgumentNumber(command,ifNeeded);
        if(!command.getArgumentRefAt(0).isALitteralString())
        {
            ifNeeded.message = "juste un chemin vers l'image";
            throw ifNeeded;
        }
        content+="<img src=\"";
        content+= command.getArgumentRefAt(0).getLitteralRef();
        content+="\"/>";
    }
    else if(command.GetName()=="ForceSaut")
    {
        if(paragraphopened)
            content+="</p>\n<p>";
        else if(paragraphallowed)
            content+="<p>&nbsp;</p>";
        else
            content+="<br />";
    }
    else
    {
        openParagraph(content);
        if(command.GetName()=="Italique")
            SurroundContent(content,command,ifNeeded,"<em>", "</em>");
        else if(command.GetName()=="Souligné")
            SurroundContent(content,command,ifNeeded,"<u>", "</u>");
        else if(command.GetName()=="StyleCésar")
            SurroundContent(content,command,ifNeeded,"<span style=\"font-family: 'Caesar Dressing'\">", "</span>");
        else if(command.GetName()=="StyleStonehenge")
            SurroundContent(content,command,ifNeeded,"<span style=\"font-family: 'Stonehenge'\">", "</span>");
        else if(command.GetName()=="StyleAtlante")
            SurroundContent(content,command,ifNeeded,"<span style=\"font-family: 'Pegasus Normal'\">", "</span>");
        else if(command.GetName()=="StyleMagie")
            SurroundContent(content,command,ifNeeded,"<span style=\"font-family: 'Magic School Two'\">", "</span>");
        else if(command.GetName()=="StyleAncien")
            SurroundContent(content,command,ifNeeded,"<span style=\"font-family: 'Parchment'\">", "</span>");
        else if(command.GetName()=="StyleMystère")
            SurroundContent(content,command,ifNeeded,"<span style=\"font-family: 'Mystery Quest'\">", "</span>");
        else if(command.GetName()=="StylePoP")
            SurroundContent(content,command,ifNeeded,"<span style=\"font-family: 'PrinceofPersia'\">", "</span>");
    }

}

void ContentFormatter::FormatFromNodeList(std::string &content, ParserNode &node)
{
    size_t i,t;
    t=node.size();
    for(i=0;i<t;i++)
    {
        if(node[i].isACommand())
            FormatFromCommand(content,node[i].getCommandRef(),node[i].makeException(""));
        else
            FormatFromLitteralString(content,node[i].getLitteralRef());
    }
}

void ContentFormatter::FormatFromLitteralString(std::string &content, std::string &litteral)
{
    QString lit = QString::fromStdString(litteral);
    QRegularExpression lineBreak("\\r?\\n[\\s*\\r?\\n]+");
    QStringList lines = lit.split(lineBreak,QString::SkipEmptyParts);
    if(paragraphallowed)
    {
        size_t i,t;
        t=lines.size();
        if(!paragraphopened)
        {
            paragraphopened=true;
            content+="<p>";
        }
        for(i=0;i<t;i++)
        {
            if((i>0)&&(i!=t-1))
            {
                content+="</p>\n<p>";
                paragraphopened=true;
            }
            content+=lines[i].toStdString();
        }
    }
    else
    {
        content+=lines.join("<br />").toStdString();
    }
}

void ContentFormatter::FormatFromNode(std::string &content, ParserNode &node)
{
    if(node.isACommand())
        FormatFromCommand(content,node.getCommandRef(),node.makeException(""));
    else if(node.isALitteralString())
        FormatFromLitteralString(content,node.getLitteralRef());
    else
        FormatFromNodeList(content,node);
}

void ContentFormatter::SurroundContent(std::string &content, ParserCommand &cmd, ParserException ifNeeded, std::string opening, std::string end)
{
    if(paragraphopened)
    {
        paragraphallowed=false;
        CheckCommandArgumentNumber(cmd,ifNeeded);
        content+=opening;
        FormatFromNode(content,cmd.getArgumentRefAt(0));
        content+=end;
        paragraphallowed=true;
    }
    else
    {
        CheckCommandArgumentNumber(cmd,ifNeeded);
        content+=opening;
        FormatFromNode(content,cmd.getArgumentRefAt(0));
        content+=end;
    }

}

void ContentFormatter::SurroundNonParagraphContent(std::string &content, ParserCommand &cmd, ParserException ifNeeded, std::string opening, std::string end)
{
    paragraphallowed=false;
    CheckCommandArgumentNumber(cmd,ifNeeded);
    content+=opening;
    FormatFromNode(content,cmd.getArgumentRefAt(0));
    content+=end;
    paragraphallowed=true;
}

void ContentFormatter::FormatAsChoiceItem(std::string &content, size_t choice)
{
    content="<div class=\"ChoiceItem\"><a href=\""+QString::number(choice).toStdString()+"\">"+content;
    content+="</a></div>";


}

void ContentFormatter::AddChoiceItemPuce(std::string &content)
{
    content+= "<span style=\"font-family: 'BlackChancery'\">+</span>";
}

void ContentFormatter::FormatAsChoiceDescription(std::string &content)
{
    content="<div class=\"ChoiceDescription\">"+content;
    content+="</div>";
}

void ContentFormatter::FormatAsChoice(std::string &content)
{
    content="<div class=\"Choice\"><p>&nbsp;</p>"
            //"<center><span style=\"font-family: 'BlackChancery'\">*****</span>"
            "<center><img src=\":/images/séparateurs/s9.png\" /></center>"
            "<p>&nbsp;</p>"
            +content;
    content+="</div>";
}

std::string ContentFormatter::FormatAsNoChoice()
{
    return "<p>&nbsp;</p><div class=\"NoChoice\"><center><img src=\":/images/séparateurs/s9.png\" /></center><p>&nbsp;</p><center><a href=\"next\">Et ensuite</a></center></div>";
}

void ContentFormatter::FormatAsContent(std::string &content)
{
    content="<div class=\"Content\">"+content;
    content+="</div>";
}

void ContentFormatter::FinalFormat(std::string &content, MemState &mem)
{
    closeParagraphIfNeeded(content);
    //content+="<&><:>"+command.getArgumentRefAt(0).getLitteralRef()+"<&>";
    QString qContent=QString::fromStdString(content);
    QStringList qLines = qContent.split("<&>");
    int i,t;
    t=qLines.size();
    for(i=0;i<t;i++)
    {
        if(qLines[i].endsWith("<:>"))
        {
            qLines[i].resize(qLines[i].size()-3);
            qLines[i]=
                    QString::fromStdString(
                        MiniScript::getMS()->getContext()->lirestring(
                            qLines[i].toStdString()));
        }

    }
    content = qLines.join("").toStdString();

}

std::string ContentFormatter::TitlePage(std::string title, std::string subtitle, std::string author)
{
    std::string res;
    res="<center><img src=\":/icone/Logo.png\" /></center>\n";
    res+="<center><h1>"+title+"</h1></center>\n";
    if(!subtitle.empty())
        res+="<center><h2>"+subtitle+"</h2></center>\n";
    res+="<center><p><em>"+author+"</em></p></center>\n";
    res+="<center><img src=\":/images/séparateurs/s5.png\" /></center>\n";
    res+="<center><p><a href=\"start\">Commencer l'aventure</a></p></center>\n";
    return res;
}

std::string ContentFormatter::EndPage(std::string title, std::string subtitle, std::string author)
{
    std::string res;
    res="<center><img src=\":/icone/Logo.png\" /></center>\n";
    res+="<h1>Fin</h1>\n";
    res+="<p> Merci d'avoir joué à l'aventure "+title;
    if(subtitle!="")
        res+="  <em>"+subtitle+"</em>";
    res+=" de\n";
    res+="<em>"+author+"</em></p>\n";
    return res;
}

std::string ContentFormatter::Accueil(QStringList files)
{
    QString res;
    res="<center><h1>Projet <img src=\":/icone/LogoSmall.png\" />tlantide</h1></center>\n";
    res+="<center><img src=\":/images/séparateurs/s5.png\" /></center>\n";

    for(QString file:files)
    {
        res+="<center  style=\"vertical-align:super\"><h2 ><img src=\":/images/ornaments/e1r.png\" /><a href=\"+"+file+"\"> "
                +file
                +" </a><img src=\":/images/ornaments/e1l.png\" /></h2></center>";
    }

    return res.toStdString();
}

void ContentFormatter::ApplyRender(std::string &content, int width)
{
    std::string style;
    style="div.ChoiceDescription{font-style:oblique}";
    style+="\nbody{font-family: 'Alegreya';}";
    //style+="\ndiv.Content{margin-left: "
    style+="\nbody{margin-left: "
            +QString::number(int(width*0.05)).toStdString()+
            "px;margin-right: "+QString::number(int(width*0.05)).toStdString()+"px;}";
    style+="\ndiv.Choice{margin-left: "
            +QString::number(int(width*0.1)).toStdString()+
            "px;margin-right: "+QString::number(int(width*0.1)).toStdString()+"px;}";
    style+="\ndiv.ChoiceItem{margin-left: "
            +QString::number(int(width*0.15)).toStdString()+
            "px;margin-right: "+QString::number(int(width*0.05)).toStdString()+"px;}";
    style+="\na{text-decoration:none;color:#000;}";
    style+="\nh1{margin-left: "
            +QString::number(int(width*0.05)).toStdString()+
            "px;margin-right: "+QString::number(int(width*0.05)).toStdString()+"px;"
            "font-family: 'Old London';font-weight:normal;}";
    style+="\nh2{margin-left: "
            +QString::number(int(width*0.05)).toStdString()+
            "px;margin-right: "+QString::number(int(width*0.05)).toStdString()+"px;"
            "font-family: 'BlackChancery';font-weight:normal;}";
    //style+="\ndiv.Content{font-style:oblique;font-family: 'Celticmd';}";
    //style+="\ndiv.Choicepuce{font-family: 'Celticmd';}";
    style = "<style>"+style+"</style>";
    content="<body>"+content;
    content+="</body>";
    content="<html><header>"+style+"</header>"+content+"</html>";
}

void ContentFormatter::openParagraph(std::string& content)
{
    if(paragraphallowed)
    {
        if(!paragraphopened)
        {
            content+="<p>";
            paragraphopened=true;
        }
    }
}

void ContentFormatter::CheckCommandArgumentNumber(ParserCommand &cmd, ParserException ex, size_t argnb)
{
    if(cmd.GetNumberArguments()!=argnb)
    {
        ex.message=cmd.GetName()+" has only "+QString::number(argnb).toStdString()+" argument(s)";
        throw ex;
    }
}

void ContentFormatter::closeParagraphIfNeeded(std::string &content)
{
    if(paragraphopened)
        content+="</p>\n";
}

}
