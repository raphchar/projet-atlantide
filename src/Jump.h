//Projet Atlantide
//    Copyright (C) 2016  Raphaël Charrondière
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#ifndef JUMP_H
#define JUMP_H
#include <string>
#include "Fact.h"
#include <map>
#include <vector>

namespace StoriesAreMyHistory{
	
class Jump{
public:
	Jump(  );
    void setDefaultJump( std::string target );
	void addConditionalJump( std::string target, std::vector< Fact > conditions );
    void addConditionalJump( std::string target, std::vector< std::string > conditions );
    std::string getDefaultJump(  );
	std::map< std::string, std::vector< Fact > >::const_iterator begin(  );
	std::map< std::string, std::vector< Fact > > ::const_iterator end(  );
private:
	std::string defaultJump;
	std::map< std::string, std::vector< Fact > > conditionalJumps;
};
	
}

#endif
