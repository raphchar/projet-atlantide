//Projet Atlantide
//    Copyright (C) 2016  Raphaël Charrondière
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>


#include "Miniscritpt.h"
//#include "outils.h"
#include "stdfunc.h"
#include <QString>
#include <fstream>

using namespace std;

namespace MiniScript {

functionsLibrary bibliotheque;
Miniscritpt MS;


ParseErreur::ParseErreur(unsigned int lgn,unsigned int offset,string quoi):
	ligne(lgn),ligneoffset(offset),message(quoi)
{
}

unsigned int ParseErreur::getligne()
{
	return ligne;
}

unsigned int ParseErreur::getoffset()
{
	return ligneoffset;
}

string ParseErreur::geterreur()
{
	return message;
}

EvalContext::EvalContext()
{
}

EvalContext::~EvalContext()
{
}

int EvalContext::lireint(string var){ return intcontent[var]; }
bool EvalContext::lirebool(string var){ return boolcontent[var]; }
string EvalContext::lirestring(string var){ return stringcontent[var]; }
void EvalContext::ecrireint(string var, int val){ intcontent[var] = val; }
void EvalContext::ecrirebool(string var, bool val){ boolcontent[var] = val; }
void EvalContext::ecrirestring(string var, string val){ stringcontent[var] = val; }

void EvalContext::clear()
{
    intcontent.clear();
    stringcontent.clear();
    boolcontent.clear();
}

QJsonObject EvalContext::getJson()
{
    QJsonObject data;
    QJsonArray intE,stringE,boolE;
    for(auto k:intcontent)
    {
        QJsonArray entry;
        entry.push_back(QString::fromStdString(k.first));
        entry.push_back(k.second);
        intE.push_back(entry);
    }
    for(auto k:stringcontent)
    {
        QJsonArray entry;
        entry.push_back(QString::fromStdString(k.first));
        entry.push_back(QString::fromStdString(k.second));
        stringE.push_back(entry);
    }
    for(auto k:boolcontent)
    {
        QJsonArray entry;
        entry.push_back(QString::fromStdString(k.first));
        entry.push_back(k.second);
        boolE.push_back(entry);
    }
    data["Entiers"]=intE;
    data["Chaines"]=stringE;
    data["Booléens"]=boolE;
    return data;
}

void EvalContext::setFromJson(QJsonObject data)
{
    clear();

    QJsonArray intE,stringE,boolE;
    intE=data["Entiers"].toArray();
    stringE=data["Chaines"].toArray();
    boolE=data["Booléens"].toArray();
    for(auto ie:intE)
    {
        QJsonArray entry=ie.toArray();
        string ch=entry[0].toString().toStdString();
        int val = entry[1].toInt();
        intcontent[ch]=val;
    }
    for(auto be:boolE)
    {
        QJsonArray entry=be.toArray();
        string ch=entry[0].toString().toStdString();
        bool val = entry[1].toInt();
        boolcontent[ch]=val;
    }
    for(auto se:stringE)
    {
        QJsonArray entry=se.toArray();
        string ch=entry[0].toString().toStdString();
        string val = entry[1].toString().toStdString();
        stringcontent[ch]=val;
    }
}

Miniscritpt::Miniscritpt(void)
{
	this->environnement = new EvalContext();
}


Miniscritpt::~Miniscritpt(void)
{
	delete this->environnement;
}

void Miniscritpt::AjouterScript(string librairie)
{
	MiniscriptParser parser;
	ifstream is(librairie);
	string line, fin;
	while (getline(is, line))
	{
		fin += line;
		fin += "\n";
	}
    parser.parse(fin);
}

bool Miniscritpt::Call(string fonction)
{
	if (!bibliotheque.exist(fonction))return false;
	if (!bibliotheque.getPrototype(fonction).empty())return false;
	bibliotheque.get(fonction)->call(this->environnement);
	return true;
}

bool Miniscritpt::Call(string fonction, int a1)
{
	if (!bibliotheque.exist(fonction))return false;
	MSFunction* f = bibliotheque.get(fonction);
	vector<MSArgument> p = { MSArgument::Nombre };
	if (f->getPrototype() != p)return false;
	f->passArg(0, a1, this->environnement);
	f->call(this->environnement);
	return true;
}

bool Miniscritpt::Call(string fonction, int a1,int a2)
{
	if (!bibliotheque.exist(fonction))return false;
	MSFunction* f = bibliotheque.get(fonction);
	vector<MSArgument> p = { MSArgument::Nombre, MSArgument::Nombre };
	if (f->getPrototype() != p)return false;
	f->passArg(0, a1, this->environnement);
	f->passArg(1, a2, this->environnement);
	f->call(this->environnement);
	return true;
}

bool Miniscritpt::Call(string fonction, string a1)
{
	if (!bibliotheque.exist(fonction))return false;
	MSFunction* f = bibliotheque.get(fonction);
	vector<MSArgument> p = { MSArgument::Chaine };
	if (f->getPrototype() != p)return false;
	f->passArg(0, a1, this->environnement);
	f->call(this->environnement);
    return true;
}

EvalContext *Miniscritpt::getContext()
{
    return  environnement;
}

void MiniscriptParser::parse(string entree)
{
	parseme(entree);
	checkvalidity();
}

void MiniscriptParser::parseme(string entree)
{
	corps.clear();
	unsigned int avancee=0;
	unsigned int ligne=1;
	unsigned int ligneoffset=0;
	unsigned int t=entree.length();
	Lexeme lex;
	for(avancee=0;avancee<t;avancee++)
	{
		switch (entree[avancee])
		{
		case '\n':
			ligne++;
			ligneoffset=0;
			lex.numligne = 0;
			break;
		case '+':
			lex.lt=PLUS;
			lex.valeur='+';
			lex.ligneoffset=ligneoffset;
			lex.numligne=ligne;
			ligneoffset++;
			break;
		case '-':
			lex.lt=MOINS;
			lex.valeur='-';
			lex.ligneoffset=ligneoffset;
			lex.numligne=ligne;
			ligneoffset++;
			break;
		case '/':
			lex.lt=DIVISE;
			lex.valeur='/';
			lex.ligneoffset=ligneoffset;
			lex.numligne=ligne;
			ligneoffset++;
			break;
		case '*':
			lex.lt=FOIS;
			lex.valeur='*';
			lex.ligneoffset=ligneoffset;
			lex.numligne=ligne;
			ligneoffset++;
			break;
		case '!':
			lex.lt=NON;
			lex.valeur='!';
			lex.ligneoffset=ligneoffset;
			lex.numligne=ligne;
			ligneoffset++;
			break;
		case '&':
			lex.lt=ET;
			lex.valeur='&';
			lex.ligneoffset=ligneoffset;
			lex.numligne=ligne;
			ligneoffset++;
			break;
		case ':':
			lex.lt=EXEC;
			lex.valeur=':';
			lex.ligneoffset=ligneoffset;
			lex.numligne=ligne;
			ligneoffset++;
			break;
		case '{':
			lex.lt=BLOCOUVRIR;
			lex.valeur='{';
			lex.ligneoffset=ligneoffset;
			lex.numligne=ligne;
			ligneoffset++;
			break;
		case '}':
			lex.lt=BLOCFERMER;
			lex.valeur='}';
			lex.ligneoffset=ligneoffset;
			lex.numligne=ligne;
			ligneoffset++;
			break;
		case '[':
			lex.lt=FONCTIONOUVRE;
			lex.valeur='[';
			lex.ligneoffset=ligneoffset;
			lex.numligne=ligne;
			ligneoffset++;
			break;
		case ']':
			lex.lt=FONCTIONFERME;
			lex.valeur=']';
			lex.ligneoffset=ligneoffset;
			lex.numligne=ligne;
			ligneoffset++;
			break;
		case ' ':
		case '\t':
        case '\r':
		case '(':
		case ')':
		case ';':
			ligneoffset++;
			lex.numligne = 0;
			break;
		case '=':
			if(avancee+1!=t && entree[avancee+1]=='=')
			{
				avancee++;
				lex.lt=EGAL;
				lex.valeur="==";
				lex.ligneoffset=ligneoffset;
				lex.numligne=ligne;
				ligneoffset+=2;
			}
			else
			{
				lex.lt=ATTR;
				lex.valeur='=';
				lex.ligneoffset=ligneoffset;
				lex.numligne=ligne;
				ligneoffset++;
			}
			break;
		case '?':
			if(avancee+1!=t && entree[avancee+1]=='=')
			{
				avancee++;
				lex.lt=COPIEBOOL;
				lex.valeur="?=";
				lex.ligneoffset=ligneoffset;
				lex.numligne=ligne;
				ligneoffset+=2;
			}
			else
			{
				lex.lt=CONDITION;
				lex.valeur='?';
				lex.ligneoffset=ligneoffset;
				lex.numligne=ligne;
				ligneoffset++;
			}
			break;
		case '|':
			if(avancee+1!=t && entree[avancee+1]=='=')
			{
				avancee++;
				lex.lt=COPIENOMBRE;
				lex.valeur="|=";
				lex.ligneoffset=ligneoffset;
				lex.numligne=ligne;
				ligneoffset+=2;
			}
			else if(avancee+1!=t && entree[avancee+1]=='>')
			{
				avancee++;
				lex.lt=NOMBREENCHAINE;
				lex.valeur="|>";
				lex.ligneoffset=ligneoffset;
				lex.numligne=ligne;
				ligneoffset+=2;
			}
			else
			{
				lex.lt=OU;
				lex.valeur='|';
				lex.ligneoffset=ligneoffset;
				lex.numligne=ligne;
				ligneoffset++;
			}
			break;
		case '>':
			if(avancee+1!=t && entree[avancee+1]=='=')
			{
				avancee++;
				lex.lt=SUPERIEUR;
				lex.valeur=">=";
				lex.ligneoffset=ligneoffset;
				lex.numligne=ligne;
				ligneoffset+=2;
			}
			else if(avancee+1!=t && entree[avancee+1]=='|')
			{
				avancee++;
				lex.lt=CHAINEENNOMBRE;
				lex.valeur=">|";
				lex.ligneoffset=ligneoffset;
				lex.numligne=ligne;
				ligneoffset+=2;
			}
			else if(avancee+1!=t && entree[avancee+1]=='>')
			{
				avancee++;
				lex.lt=CONCAT;
				lex.valeur=">>";
				lex.ligneoffset=ligneoffset;
				lex.numligne=ligne;
				ligneoffset+=2;
			}
			else if (avancee + 1 != t && entree[avancee + 1] == '*')
			{
				avancee++;
                lex.lt = COPIECHAINE;
				lex.valeur = ">*";
				lex.ligneoffset = ligneoffset;
				lex.numligne = ligne;
				ligneoffset += 2;
			}
			else
			{
				throw ParseErreur(ligne,ligneoffset,"L'opérateur > n'existe pas");
			}
			break;
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			lex.lt=NOMBRE;
			lex.numligne=ligne;
			lex.ligneoffset=ligneoffset;
			ligneoffset++;
			lex.valeur=entree[avancee];
			while(avancee+1!=t && entree[avancee+1]<='9'&&entree[avancee+1]>='0')
			{
				avancee++;
				ligneoffset++;
				lex.valeur+=entree[avancee];
			}
			break;
		case 'a':
		case 'b':
		case 'c':
		case 'd':
		case 'e':
		case 'f':
		case 'g':
		case 'h':
		case 'i':
		case 'j':
		case 'k':
		case 'l':
		case 'm':
		case 'n':
		case 'o':
		case 'p':
		case 'q':
		case 'r':
		case 's':
		case 't':
		case 'u':
		case 'v':
		case 'w':
		case 'x':
		case 'y':
		case 'z':
		case 'A':
		case 'B':
		case 'C':
		case 'D':
		case 'E':
		case 'F':
		case 'G':
		case 'H':
		case 'I':
		case 'J':
		case 'K':
		case 'L':
		case 'M':
		case 'N':
		case 'O':
		case 'P':
		case 'Q':
		case 'R':
		case 'S':
		case 'T':
		case 'U':
		case 'V':
		case 'W':
		case 'X':
		case 'Y':
		case 'Z':
			lex.lt=VARIABLE;
			lex.numligne=ligne;
			lex.ligneoffset=ligneoffset;
			lex.valeur=entree[avancee];
			ligneoffset++;
			while(avancee+1!=t &&
				((entree[avancee+1]<='9'&&entree[avancee+1]>='0')||
				(entree[avancee+1]<='z'&&entree[avancee+1]>='a')||
				(entree[avancee+1]<='Z'&&entree[avancee+1]>='A')))
			{
				avancee++;
				lex.valeur+=entree[avancee];
				ligneoffset++;
			}
			break;
		case '"':
			lex.lt=CHAINE;
			lex.numligne=ligne;
			lex.ligneoffset=ligneoffset;
			lex.valeur="";
			ligneoffset++;
			while(avancee+1!=t && entree[avancee+1] !='"')
			{
				avancee++;
				if(entree[avancee]=='\\')
				{
					avancee++;
					ligneoffset++;
					switch(entree[avancee])
					{
					case '"':
						lex.valeur+='"';
						break;
					case 'n':
						lex.valeur+='\n';
						break;
					case 't':
						lex.valeur+='t';
						break;
					default:
						throw ParseErreur(ligne,ligneoffset,"Le caractère suivant n'existe pas \\"+entree[avancee]);
					}
				}
				else if(entree[avancee]=='\\')
					throw ParseErreur(ligne,ligneoffset,
					"Les sauts de ligne ne sont pas autorisés au milieu d'une chaine de caractères");
				else
					lex.valeur+=entree[avancee];
				ligneoffset++;
			}
			avancee++;
			break;
		default:
			throw ParseErreur(ligne,ligneoffset,"Je ne vois pas de quoi vous voulez parlez");
		}
		if (lex.numligne!=0)
			corps.push_back(lex);
	}
}

void MiniscriptParser::checkvaliditybool()
{
	switch (corps[vali].lt)
	{
	case NON:
		vali++;
		checkvaliditybool();
		break;
	case EGAL:
	case SUPERIEUR:
		vali++;
		checkvaliditynumber();
		checkvaliditynumber();
		break;
	case ET:
	case OU:
		vali++;
		checkvaliditybool();
		checkvaliditybool();
		break;
	case VARIABLE:
		vali++;
		break;
	default:
		throw ParseErreur(corps[vali].numligne, corps[vali].ligneoffset,"On devrait trouver un booléen");
	}
}

void MiniscriptParser::checkvaliditychaine()
{
	switch (corps[vali].lt)
	{
	case CONCAT:
		vali++;
		checkvaliditychaine();
		checkvaliditychaine();
		break;
	case NOMBREENCHAINE:
		vali++;
		checkvaliditynumber();
		break;
	case VARIABLE:
	case CHAINE:
		vali++;
		break;
	default:
		throw ParseErreur(corps[vali].numligne, corps[vali].ligneoffset, "On devrait trouver une chaine");
	}
}

void MiniscriptParser::checkvaliditynumber()
{
	switch (corps[vali].lt)
	{
	case PLUS:
	case FOIS:
	case MOINS:
	case DIVISE:
		vali++;
		checkvaliditynumber();
		checkvaliditynumber();
		break;
	case CHAINEENNOMBRE:
		vali++;
		checkvaliditychaine();
		break;
	case VARIABLE:
	case NOMBRE:
		vali++;
		break;
	default:
		throw ParseErreur(corps[vali].numligne, corps[vali].ligneoffset, "On devrait trouver un nombre");
	}
}

void MiniscriptParser::checkvalidityexec()
{
	switch (corps[vali].lt)
	{
	case CONDITION:
		vali++;
		checkvaliditybool();
		if (corps[vali].lt != BLOCOUVRIR)
			throw ParseErreur(corps[vali].numligne, corps[vali].ligneoffset, "On devrait trouver {");
		vali++;
		while (corps[vali].lt !=BLOCFERMER)
		{
			checkvalidityexec();
		}
		vali++;
		if (corps[vali].lt != BLOCOUVRIR)
			throw ParseErreur(corps[vali].numligne, corps[vali].ligneoffset, "On devrait trouver {");
		vali++;
		while (corps[vali].lt != BLOCFERMER)
		{
			checkvalidityexec();
		}
		vali++;
		break;
	case COPIENOMBRE:
		vali++;
		if (corps[vali].lt != VARIABLE)
			throw ParseErreur(corps[vali].numligne, corps[vali].ligneoffset, "On devrait trouver une variable");
		vali++;
		checkvaliditynumber();
		break;
	case COPIEBOOL:
		vali++;
		if (corps[vali].lt != VARIABLE)
			throw ParseErreur(corps[vali].numligne, corps[vali].ligneoffset, "On devrait trouver une variable");
		vali++;
		checkvaliditybool();
		break;
	case COPIECHAINE:
		vali++;
		if (corps[vali].lt != VARIABLE)
			throw ParseErreur(corps[vali].numligne, corps[vali].ligneoffset, "On devrait trouver une variable");
		vali++;
		checkvaliditychaine();
		break;
	case EXEC:
		vali++;
		checkvalidityfonctioncall();
		break;
	default:
		throw ParseErreur(corps[vali].numligne, corps[vali].ligneoffset, "On devrait trouver une instruction");
	}
}

void MiniscriptParser::checkvalidityfonction()
{
	string fname;
	vector<MSArgument> prototype;
	vector<string> argsNames;
	vector<Lexeme> fcontent;
	unsigned int start;
	if (corps[vali].lt != FONCTIONFERME)
		throw ParseErreur(corps[vali].numligne, corps[vali].ligneoffset, "On devrait trouver une fonction");
	vali++;
	if (corps[vali].lt != VARIABLE)//Nom
		throw ParseErreur(corps[vali].numligne, corps[vali].ligneoffset, "On devrait trouver un argument");
	else
		fname = corps[vali].valeur;
	if (bibliotheque.isstd(fname)||bibliotheque.exist(fname))
		throw ParseErreur(corps[vali].numligne, corps[vali].ligneoffset, "La fonction "+fname+" existe déja");
	vali++;
	while (corps[vali].lt != FONCTIONOUVRE)
	{
		if ((corps[vali].lt != CONDITION) && (corps[vali].lt != OU) && (corps[vali].lt != CONCAT))
			throw ParseErreur(corps[vali].numligne, corps[vali].ligneoffset, "On devrait trouver un type");
		else
		{
			switch (corps[vali].lt)
			{
			case CONDITION:
				prototype.push_back(MSArgument::Booleen);
				break;
			case OU:
				prototype.push_back(MSArgument::Nombre);
				break;
			default:
				prototype.push_back(MSArgument::Chaine);
			}
		}
		vali++;
		if (corps[vali].lt != VARIABLE)
			throw ParseErreur(corps[vali].numligne, corps[vali].ligneoffset, "On devrait trouver un argument");
		else
			argsNames.push_back(corps[vali].valeur);
		vali++;
	}
	vali++;
	if (corps[vali].lt != BLOCOUVRIR)
		throw ParseErreur(corps[vali].numligne, corps[vali].ligneoffset, "On devrait trouver {");
	vali++;
	start = vali;
	while (corps[vali].lt != BLOCFERMER)
	{
		checkvalidityexec();
	}
	for (; start < vali; start++)
		fcontent.push_back(corps[start]);
	bibliotheque.add(MSFunction(fname, prototype, argsNames, fcontent));
	vali++;
}

void MiniscriptParser::checkvalidityfonctioncall()
{
	string fname = corps[vali].valeur;
	if (bibliotheque.isstd(fname))
	{
		vector<MSArgument> proto = bibliotheque.getStdPrototype(fname);
		vali++;
		for (unsigned int i = 0; i < proto.size(); i++)
		{
			switch (proto[i])
			{
			case MSArgument::Booleen:
				checkvaliditybool();
				break;
			case MSArgument::Chaine:
				checkvaliditychaine();
				break;
			case MSArgument::Nombre:
				checkvaliditynumber();
				break;
			}
		}
	}
	else
	{
		if (!bibliotheque.exist("name"))
			throw ParseErreur(corps[vali].numligne, corps[vali].ligneoffset, "La fonction "+fname+"n'est pas définie");
		vector<MSArgument> proto = bibliotheque.getPrototype(fname);
		vali++;
		for (unsigned int i = 0; i < proto.size(); i++)
		{
			switch (proto[i])
			{
			case MSArgument::Booleen:
				checkvaliditybool();
				break;
			case MSArgument::Chaine:
				checkvaliditychaine();
				break;
			case MSArgument::Nombre:
				checkvaliditynumber();
				break;
			}
		}
	}
}

void MiniscriptParser::checkvalidity()
{
	vali = 0;
	valt = this->corps.size();
	while (vali != valt)
	{
		try
		{
			checkvalidityfonction();
		}
		catch (ParseErreur e)
		{
            throw e;
		}
		catch (...)
		{
            throw ParseErreur(0,0,"} ou [ oublié");
		}
	}
}

void MSFunction::call(EvalContext* env)
{
	vali = 0;
	valt = this->Corps.size();
	while (vali!=valt)
		this->eval(env);
}

bool MSFunction::evalbool(EvalContext* env)
{
	bool b1;
	int i1;
	switch (Corps[vali].lt)
	{
	case NON:
		vali++;
		return !evalbool(env);
		break;
	case EGAL:
		vali++;
		return (evalnumber(env) == evalnumber(env));
	case SUPERIEUR:
		vali++;
		i1 = evalnumber(env);
		return ( i1 >= evalnumber(env));
	case ET:
		vali++;
		return (evalbool(env) && evalbool(env));
	case OU:
		vali++;
		return (evalbool(env) || evalbool(env));
	case VARIABLE:
		b1=env->lirebool(Corps[vali].valeur);
		vali++;
		return b1;
	default:
		throw ParseErreur(Corps[vali].numligne, Corps[vali].ligneoffset, "Inside Function Unexpected error! On devrait trouver un booléen");
	}
	return false;
}

string MSFunction::evalchaine(EvalContext* env)
{
	string s1;
	switch (Corps[vali].lt)
	{
	case CONCAT:
		vali++;
		s1 = evalchaine(env);
		return s1 + evalchaine(env);
	case NOMBREENCHAINE:
		vali++;
        return QString::number(evalnumber(env)).toStdString();
	case VARIABLE:
		s1 = env->lirestring(Corps[vali].valeur);
		vali++;
		return s1;
	case CHAINE:
		s1 = Corps[vali].valeur;
		vali++;
		return s1;
	default:
		throw ParseErreur(Corps[vali].numligne, Corps[vali].ligneoffset, "Inside Function Unexpected Error: On devrait trouver une chaine");
	}
	return "";
}

int MSFunction::evalnumber(EvalContext* env)
{
	int i1;
	switch (Corps[vali].lt)
	{
	case PLUS:
		vali++;
		return (evalnumber(env) + evalnumber(env));
	case FOIS:
		vali++;
		return (evalnumber(env) * evalnumber(env));
	case MOINS:
		vali++;
		i1 = evalnumber(env);
		return (i1 - evalnumber(env));
	case DIVISE:
		vali++;
		i1 = evalnumber(env);
		return (i1 / evalnumber(env));
	case CHAINEENNOMBRE:
		vali++;
        return QString::fromStdString(evalchaine(env)).toInt();
		break;
	case VARIABLE:
		i1 = env->lireint( Corps[vali].valeur);
		vali++;
		return i1;
	case NOMBRE:
        i1 = QString::fromStdString(Corps[vali].valeur).toInt();
		vali++;
		return i1;
		break;
	default:
		throw ParseErreur(Corps[vali].numligne, Corps[vali].ligneoffset, "Inside Function Unexpected Error: On devrait trouver un nombre");
	}
}


void MSFunction::eval(EvalContext* env)
{
	bool b1;
	int i1;
	string s1;
	switch (Corps[vali].lt)
	{
	case CONDITION:
		vali++;
		b1=evalbool(env);
		vali++;//{
		if (b1)
		{
			while (Corps[vali].lt != BLOCFERMER)
			{
				eval(env);
			}
			vali+=2;
			i1 = 1;
			while (i1 != 0)
			{
				if (Corps[vali].lt == BLOCFERMER)i1--;
				else if (Corps[vali].lt == BLOCOUVRIR)i1++;
				vali++;
			}
		}
		else
		{
			i1 = 1;
			while (i1 != 0)
			{
				if (Corps[vali].lt == BLOCFERMER)i1--;
				else if (Corps[vali].lt == BLOCOUVRIR)i1++;
				vali++;
			}
			vali++;
			while (Corps[vali].lt != BLOCFERMER)
			{
				eval(env);
			}
			vali++;
		}
		return;
	case COPIENOMBRE:
		vali++;
		s1 = Corps[vali].valeur;
		vali++;
		env->ecrireint(s1,evalnumber(env));
		return;
	case COPIEBOOL:
		vali++;
		s1 = Corps[vali].valeur;
		vali++;
		env->ecrirebool( s1, evalbool(env));
		return;
	case COPIECHAINE:
		vali++;
		s1 = Corps[vali].valeur;
		vali++;
		env->ecrirestring( s1, evalchaine(env));
		return;
	case EXEC:
		vali++;
		evalfun(env);
		break;
	default:
		throw ParseErreur(Corps[vali].numligne, Corps[vali].ligneoffset, "Inside Fonction Unexpected Error: On devrait trouver une instruction");
	}
}

void MSFunction::evalfun(EvalContext* env)
{
	string fname = Corps[vali].valeur;
	if (bibliotheque.isstd(fname))
	{
		vector<MSArgument> proto = bibliotheque.getStdPrototype(fname);
		vali++;
		for (unsigned int i = 0; i < proto.size(); i++)
		{
			switch (proto[i])
			{
			case MSArgument::Booleen:
				bibliotheque.passStdArg(i, evalbool(env), env);
				break;
			case MSArgument::Chaine:
				bibliotheque.passStdArg(i, evalchaine(env), env);
				break;
			case MSArgument::Nombre:
				bibliotheque.passStdArg(i, evalnumber(env), env);
				break;
			}
		}
		bibliotheque.stdcall(fname, env);
	}
	else
	{
		if (!bibliotheque.exist("name"))
			throw ParseErreur(Corps[vali].numligne, Corps[vali].ligneoffset, "Unexpected Inside Error: La fonction " + fname + "n'est pas définie");
		MSFunction* fun = bibliotheque.get(fname);
		vector<MSArgument> proto = fun->getPrototype();
		vali++;
		for (unsigned int i = 0; i < proto.size(); i++)
		{
			switch (proto[i])
			{
			case MSArgument::Booleen:
				fun->passArg(i, evalbool(env), env);
				break;
			case MSArgument::Chaine:
				fun->passArg(i, evalchaine(env), env);
				break;
			case MSArgument::Nombre:
				fun->passArg(i, evalnumber(env), env);
				break;
			}
		}
		fun->call(env);
	}
}

vector<MSArgument> MSFunction::getPrototype()
{
	return this->Prototype;
}

string MSFunction::getName()
{
	return this->Name;
}


void MSFunction::passArg(unsigned int num, int val, EvalContext* environment)
{
	if (this->Prototype[num] != MSArgument::Nombre)
		throw "Prototype incorrect";
	environment->ecrireint(this->ArgumentName[num], val);
}

void MSFunction::passArg(unsigned int num, string val, EvalContext* environment)
{
	if (this->Prototype[num] != MSArgument::Chaine)
		throw "Prototype incorrect";
	environment->ecrirestring(this->ArgumentName[num], val);
}

void MSFunction::passArg(unsigned int num, bool val, EvalContext* environment)
{
	if (this->Prototype[num] != MSArgument::Booleen)
		throw "Prototype incorrect";
	environment->ecrirebool(this->ArgumentName[num], val);
}

MSFunction::MSFunction(string name, vector<MSArgument> prototype, vector<string> argsnames, vector<Lexeme>corps)
	:Name(name), Prototype(prototype), ArgumentName(argsnames), Corps(corps)
{}

MSFunction::MSFunction()
{}

vector<MSArgument> functionsLibrary::getPrototype(string name)
{
	return this->content[name].getPrototype();
}

bool functionsLibrary::exist(string name)
{
	return (this->content.find(name) != this->content.end());
}

void functionsLibrary::add(MSFunction f)
{
	this->content[f.getName()] = f;
}

MSFunction* functionsLibrary::get(string name)
{
	return &(this->content[name]);
}

bool functionsLibrary::isstd(string name)
{
	return (this->stdlib.find(name) != this->stdlib.end());
}

vector<MSArgument> functionsLibrary::getStdPrototype(string name)
{
	return this->stdlib[name];
}

void functionsLibrary::passStdArg(unsigned int num, int val, EvalContext* environment)
{
    string var = "_" + QString::number(num).toStdString();
	environment->ecrireint(var, val);
}

void functionsLibrary::passStdArg(unsigned int num, string val, EvalContext* environment)
{
    string var = "_" + QString::number(num).toStdString();
	environment->ecrirestring(var, val);
}

void functionsLibrary::passStdArg(unsigned int num, bool val, EvalContext* environment)
{
    string var = "_" + QString::number(num).toStdString();
    environment->ecrirebool(var, val);
}

void functionsLibrary::clearCurrent()
{
    content.clear();
}

void functionsLibrary::stdcall(string name, EvalContext* environment)
{
	this->stdfuncs[name](environment);
}

functionsLibrary::functionsLibrary()
{
	//Texte
    /*stdlib["ChangerTexte"] = { MSArgument::Chaine, MSArgument::Chaine };
	stdlib["SupprimerTexte"] = { MSArgument::Chaine}; 
	stdlib["BougerTexte"] = { MSArgument::Chaine, MSArgument::Nombre, MSArgument::Nombre };
	stdlib["AjouterTextePlume"] = { MSArgument::Chaine, MSArgument::Chaine, MSArgument::Chaine, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre };
	stdlib["AjouterTexte"] = { MSArgument::Chaine, MSArgument::Chaine, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre };
	stdlib["AjouterTexteJoli"] = { MSArgument::Chaine, MSArgument::Chaine, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre };
	stdlib["AjouterTexteTitre"] = { MSArgument::Chaine, MSArgument::Chaine, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre };
	stdfuncs["ChangerTexte"] = ChangerTexte;
	stdfuncs["SupprimerTexte"] = SupprimerTexte;
	stdfuncs["BougerTexte"] = BougerTexte;
	stdfuncs["AjouterTextePlume"] = AjouterTextePlume;
	stdfuncs["AjouterTexte"] = AjouterTexte;
	stdfuncs["AjouterTexteJoli"] = AjouterTexteJoli;
    stdfuncs["AjouterTexteTitre"] = AjouterTexteTitre;*/

	//Images
    /*stdlib["InsererImage"] = { MSArgument::Chaine, MSArgument::Chaine, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre };
	stdlib["InsererImageAlpha"] = { MSArgument::Chaine, MSArgument::Chaine, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre };
	stdlib["BougerImage"] = { MSArgument::Chaine, MSArgument::Nombre, MSArgument::Nombre };
	stdlib["SupprimerImage"] = { MSArgument::Chaine };
	stdfuncs["InsererImage"] = InsererImage;
	stdfuncs["InsererImageAlpha"] = InsererImageAlpha;
	stdfuncs["BougerImage"] = BougerImage;
    stdfuncs["SupprimerImage"] = SupprimerImage;*/

	//Boutons
    /*stdlib["InsererBouton"] = { MSArgument::Chaine, MSArgument::Chaine, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre };
	stdlib["InsererBoutonImage"] = { MSArgument::Chaine, MSArgument::Chaine, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Chaine, MSArgument::Chaine };
	stdlib["InsererBoutonJoli"] = { MSArgument::Chaine, MSArgument::Chaine, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre };
	stdlib["InsererBoutonJoliImage"] = { MSArgument::Chaine, MSArgument::Chaine, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Chaine, MSArgument::Chaine };
	stdlib["InsererBoutonTitre"] = { MSArgument::Chaine, MSArgument::Chaine, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre };
	stdlib["InsererBoutonTitreImage"] = { MSArgument::Chaine, MSArgument::Chaine, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Chaine, MSArgument::Chaine };
	stdlib["InsererBoutonPLume"] = { MSArgument::Chaine, MSArgument::Chaine, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre };
	stdlib["InsererBoutonPlumeImage"] = { MSArgument::Chaine, MSArgument::Chaine, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Chaine, MSArgument::Chaine };
	stdlib["ChangerTexteBouton"] = { MSArgument::Chaine, MSArgument::Chaine };
	stdlib["BougerBouton"] = { MSArgument::Chaine, MSArgument::Nombre, MSArgument::Nombre };
	stdlib["SupprimerBouton"] = { MSArgument::Chaine };
	stdfuncs["InsererBouton"] = InsererBouton;
	stdfuncs["InsererBoutonImage"] = InsererBoutonImage;
	stdfuncs["InsererBoutonJoli"] = InsererBoutonJoli;
	stdfuncs["InsererBoutonJoliImage"] = InsererBoutonJoliImage;
	stdfuncs["InsererBoutonTitre"] = InsererBoutonTitre;
	stdfuncs["InsererBoutonTitreImage"] = InsererBoutonTitreImage;
	stdfuncs["InsererBoutonPLume"] = InsererBoutonPlume;
	stdfuncs["InsererBoutonPlumeImage"] = InsererBoutonPlumeImage;
	stdfuncs["ChangerTexteBouton"] = ChangerTexteBouton;
	stdfuncs["BougerBouton"] = BougerBouton;
    stdfuncs["SupprimerBouton"] = SupprimerBouton;*/

	//Saisie
    /*stdlib["ChangerTexteSaisie"] = { MSArgument::Chaine, MSArgument::Chaine };
	stdlib["SupprimerSaisie"] = { MSArgument::Chaine };
	stdlib["BougerSaisie"] = { MSArgument::Chaine, MSArgument::Nombre, MSArgument::Nombre };
	stdlib["AjouterSaisiePlume"] = { MSArgument::Chaine, MSArgument::Chaine, MSArgument::Chaine, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre };
	stdlib["AjouterSaisie"] = { MSArgument::Chaine, MSArgument::Chaine, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre };
	stdlib["AjouterSaisieJoli"] = { MSArgument::Chaine, MSArgument::Chaine, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre };
	stdlib["AjouterSaisieTitre"] = { MSArgument::Chaine, MSArgument::Chaine, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre };
	stdlib["AjouterSaisiePlumeImage"] = { MSArgument::Chaine, MSArgument::Chaine, MSArgument::Chaine, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Chaine };
	stdlib["AjouterSaisieImage"] = { MSArgument::Chaine, MSArgument::Chaine, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Chaine };
	stdlib["AjouterSaisieJoliImage"] = { MSArgument::Chaine, MSArgument::Chaine, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Chaine };
	stdlib["AjouterSaisieTitreImage"] = { MSArgument::Chaine, MSArgument::Chaine, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Chaine };
	stdlib["TexteSaisie"] = { MSArgument::Chaine, MSArgument::Chaine };
	stdfuncs["ChangerTexteSaisie"] = ChangerTexteSaisie;
	stdfuncs["SupprimerSaisie"] = SupprimerSaisie;
	stdfuncs["BougerSaisie"] = BougerSaisie;
	stdfuncs["AjouterSaisiePlume"] = AjouterSaisiePlume;
	stdfuncs["AjouterSaisie"] = AjouterSaisie;
	stdfuncs["AjouterSaisieJoli"] = AjouterSaisieJoli;
	stdfuncs["AjouterSaisieTitre"] = AjouterSaisieTitre;
	stdfuncs["AjouterSaisiePlumeImage"] = AjouterSaisiePlumeImage;
	stdfuncs["AjouterSaisieImage"] = AjouterSaisieImage;
	stdfuncs["AjouterSaisieJoliImage"] = AjouterSaisieJoliImage;
	stdfuncs["AjouterSaisieTitreImage"] = AjouterSaisieTitreImage;
    stdfuncs["TexteSaisie"] = TexteSaisie;*/

	//Global
    /*stdlib["Message"] = { MSArgument::Chaine };
	stdlib["Coller"] = { };
	stdlib["ChargerGraphiques"] = {};
	stdlib["Charger"] = { MSArgument::Chaine };
	stdlib["ChangerCouleurArrierePlan"] = { MSArgument::Nombre, MSArgument::Nombre, MSArgument::Nombre };
	stdlib["ChangerTailleFenetre"] = { MSArgument::Nombre, MSArgument::Nombre };
	stdlib["For"] = { MSArgument::Chaine, MSArgument::Nombre, MSArgument::Nombre, MSArgument::Chaine };
	stdfuncs["Message"] = Message;
	stdfuncs["Coller"] = Coller;
	stdfuncs["ChargerGraphiques"] = ChargerGraphiques;
	stdfuncs["Charger"] = Charger;
	stdfuncs["ChangerCouleurArrierePlan"] = ChangerCouleurArrierePlan;
	stdfuncs["ChangerTailleFenetre"] = ChangerTailleFenetre;
    stdfuncs["For"] = For;*/

	//Fichiers
	stdlib["FichierCharger"] = { MSArgument::Chaine };
	stdlib["FichierSauver"] = { MSArgument::Chaine };
	stdlib["FichierLireNombre"] = { MSArgument::Chaine, MSArgument::Chaine, MSArgument::Chaine };
	stdlib["FichierLireChaine"] = { MSArgument::Chaine, MSArgument::Chaine, MSArgument::Chaine };
	stdlib["FichierAjoutNombre"] = { MSArgument::Chaine, MSArgument::Chaine, MSArgument::Nombre };
	stdlib["FichierAjoutChaine"] = { MSArgument::Chaine, MSArgument::Chaine, MSArgument::Chaine };
	stdfuncs["FichierCharger"] = FichierCharger;
	stdfuncs["FichierSauver"] = FichierSauver;
	stdfuncs["FichierLireNombre"] = FichierLireNombre;
	stdfuncs["FichierLireChaine"] = FichierLireChaine;
	stdfuncs["FichierAjoutNombre"] = FichierAjoutNombre;
	stdfuncs["FichierAjoutChaine"] = FichierAjoutChaine;

    //Global
    stdlib["DemandeInfo"] = { MSArgument::Chaine, MSArgument::Chaine, MSArgument::Chaine, MSArgument::Chaine };
    stdlib["DemandeNombre"] = { MSArgument::Chaine, MSArgument::Chaine, MSArgument::Chaine, MSArgument::Chaine };
    stdlib["Message"] = { MSArgument::Chaine, MSArgument::Chaine };
    stdlib["Erreur"] = { MSArgument::Chaine, MSArgument::Chaine };
    stdlib["Avertissement"] = { MSArgument::Chaine, MSArgument::Chaine };
    stdlib["ValiderCode"] = { MSArgument::Chaine, MSArgument::Chaine, MSArgument::Chaine, MSArgument::Chaine };
    stdfuncs["DemandeInfo"] = DemandeInfo;
    stdfuncs["DemandeNombre"] = DemandeNombre;
    stdfuncs["Message"] = Message;
    stdfuncs["Erreur"] = Erreur;
    stdfuncs["Avertissement"] = Avertissement;
    stdfuncs["ValiderCode"] = ValiderCode;

}

functionsLibrary* getLibrary()
{
	return &bibliotheque;
}

Miniscritpt* getMS()
{
	return &MS;
}

}
