//Projet Atlantide
//    Copyright (C) 2016  Raphaël Charrondière
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#ifndef STDFUNC_H
#define STDFUNC_H

#include "Miniscritpt.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

namespace MiniScript {



void FichierAjoutChaine(EvalContext* environment);
void FichierAjoutNombre(EvalContext* environment);
void FichierLireChaine(EvalContext* environment);
void FichierLireNombre(EvalContext* environment);
void FichierSauver(EvalContext* environment);
void FichierCharger(EvalContext* environment);

void DemandeInfo(EvalContext* environment);
void DemandeNombre(EvalContext* environment);
void Message(EvalContext* environment);
void Erreur(EvalContext* environment);
void Avertissement(EvalContext* environment);

void ValiderCode(EvalContext* environment);

}
#endif // STDFUNC_H
