#-------------------------------------------------
#
# Project created by QtCreator 2016-03-20T08:42:58
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ProjetAtlantide
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    parser.cpp \
    Story.cpp \
    Choix.cpp \
    Fact.cpp \
    Fragment.cpp \
    Jump.cpp \
    contentformatter.cpp \
    MemState.cpp \
    Miniscritpt.cpp \
    stdfunc.cpp

HEADERS  += mainwindow.h \
    parser.h \
    Story.h \
    Choix.h \
    Fact.h \
    Fragment.h \
    Jump.h \
    contentformatter.h \
    MemState.h \
    Miniscritpt.h \
    stdfunc.h

CONFIG += mobility
MOBILITY = 

RESOURCES += \
    ressources.qrc

RC_ICONS = icone/LogoMed.ico

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

