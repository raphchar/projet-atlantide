//Projet Atlantide
//    Copyright (C) 2016  Raphaël Charrondière
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#ifndef CONTENTFORMATTER_H
#define CONTENTFORMATTER_H

#include "parser.h"
#include "MemState.h"

namespace StoriesAreMyHistory {

class ContentFormatter{
public:
    //ContentFormatter();
    void FormatFromCommand(std::string& content, ParserCommand& command, ParserException ifNeeded);
    void FormatFromNodeList(std::string& content, ParserNode& node);
    void FormatFromLitteralString(std::string& content, std::string& litteral);
    void FormatFromNode(std::string& content, ParserNode& node);
    void SurroundContent(std::string& content, ParserCommand& cmd, ParserException ifNeeded, std::string opening, std::string end);
    void SurroundNonParagraphContent(std::string& content, ParserCommand& cmd, ParserException ifNeeded, std::string opening, std::string end);
    void FormatAsChoiceItem(std::string& content,size_t choice);
    void AddChoiceItemPuce(std::string& content);
    void FormatAsChoiceDescription(std::string& content);
    void FormatAsChoice(std::string& content);
    std::string FormatAsNoChoice();
    void FormatAsContent(std::string& content);
    void FinalFormat(std::string& content,MemState& mem);
    static std::string TitlePage(std::string title, std::string subtitle, std::string author);
    static std::string EndPage(std::string title, std::string subtitle, std::string author);
    static std::string Accueil(QStringList files);
    static void ApplyRender(std::string& content, int width);
    void openParagraph(std::string& content);
private:
    static void CheckCommandArgumentNumber(ParserCommand& cmd, ParserException ex, size_t argnb=1);
    void closeParagraphIfNeeded(std::string& content);
    bool paragraphopened=false;
    bool paragraphallowed=true;
};

}

#endif // CONTENTFORMATTER_H
