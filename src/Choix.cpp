//Projet Atlantide
//    Copyright (C) 2016  Raphaël Charrondière
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#include "Choix.h"
#include "contentformatter.h"

namespace StoriesAreMyHistory {
	
ChoiceItem::ChoiceItem()
{

}

ChoiceItem::ChoiceItem(ParserCommand &cmd, ParserException ifNeeded)
{
    if(cmd.GetNumberArguments()!=2)
    {
        ifNeeded.message="A Rouage has 2 arguments";
        throw ifNeeded;
    }
    if(!cmd.getArgumentRefAt(1).isALitteralString())
        throw cmd.getArgumentRefAt(1).makeException("This should be a list");
    auto l=cmd.getArgumentRefAt(1).castLitteralAsList();
    for(auto f:l)
        this->linkedFact.push_back(Fact(f));
    ParserNode& n =cmd.getArgumentRefAt(0);
    formater.openParagraph(description);
    formater.AddChoiceItemPuce(description);
    if(n.isALitteralString())
        formater.FormatFromLitteralString(description,n.getLitteralRef());
    else if (n.isACommand())
        formater.FormatFromCommand(description,n.getCommandRef(),n.makeException(""));
    else
    {
        size_t i,t;
        t=n.size();
        for(i=0;i<t;i++)
        {
            if(n[i].isALitteralString())
                formater.FormatFromLitteralString(description,n[i].getLitteralRef());
            else
                formater.FormatFromCommand(description,n[i].getCommandRef(),n.makeException(""));
        }
    }
}

Choix::Choix()
{

}

Choix::Choix(ParserCommand &cmd, ParserException ifNeeded)
{
    if(cmd.GetNumberArguments()!=1)
    {
        ifNeeded.message="Choix command has exactly one argument";
        throw ifNeeded;
    }
    ParserNode& n=cmd.getArgumentRefAt(0);
    if(n.isALitteralString())
        throw n.makeException("Empty choice");
    if(n.isACommand())
        throw n.makeException("Just one choice is no choice");
    size_t i, t;
    t=n.size();
    std::string choicedescription;
    for(i=0;i<t;i++)
    {
        if(n[i].isALitteralString())
            formater.FormatFromLitteralString(
                        choicedescription,n[i].getLitteralRef());
        else
        {
            if(n[i].getCommandRef().GetName()=="Rouage")
            {
                ChoiceItem ci(n[i].getCommandRef(),n[i].makeException(""));
                addChoice(ci);
            }
            else
                formater.FormatFromCommand(
                            choicedescription,n[i].getCommandRef(),n[i].makeException(""));
        }
    }
    setDescription(choicedescription);
}

	
void Choix::addChoice(ChoiceItem newChoice)
{
	this->choices.push_back(newChoice);
}

void Choix::setDescription(std::string description)
{
	this->description = description;
}

void ChoiceItem::addFact(Fact newFact)
{
	this->linkedFact.push_back(newFact);
}

void ChoiceItem::setDescription(std::string description)
{
	this->description = description;
}

std::string ChoiceItem::getDescription()
{
	return this->description;
}

std::string* ChoiceItem::getDescriptionPtr()
{
	return &this->description;
}

std::string Choix::getDescription()
{
	return this->description;
}

std::string* Choix::getDescriptionPtr()
{
	return &this->description;
}

std::vector< Fact > ChoiceItem::getLinkedFacts()
{
    return this->linkedFact;
}

std::string ChoiceItem::render(MemState &mem)
{
    std::string res=description;
    formater.FinalFormat(res,mem);
    return res;
}

std::vector< ChoiceItem > Choix::getChoices()
{
	return this->choices;
}

std::vector< ChoiceItem >::const_iterator Choix::begin()
{
	return this->choices.begin();
}

std::vector< ChoiceItem >::const_iterator Choix::end()
{
    return this->choices.end();
}

std::string Choix::render(MemState &mem)
{
    std::string res=description;
    formater.FinalFormat(res,mem);
    formater.FormatAsChoiceDescription(res);
    size_t i,t;
    t=choices.size();
    for(i=0;i<t;i++)
    {
        std::string cs=choices[i].render(mem);
        formater.FormatAsChoiceItem(cs,i);
        res+=cs;
    }
    formater.FormatAsChoice(res);
    return res;
}


}
