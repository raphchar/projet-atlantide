//Projet Atlantide
//    Copyright (C) 2016  Raphaël Charrondière
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#ifndef MINISCRIPT_H
#define MINISCRIPT_H
#include <string>
#include <map>
#include <vector>
#include <exception>
#include <QJsonObject>
#include <QJsonArray>

namespace MiniScript {

class ParseErreur :
    public std::exception
{
public:
    ParseErreur(unsigned int ligne,unsigned int offset,std::string quoi);
	virtual const char* what() const throw()
     {
         std::string rep="Erreur ligne "+ligne;
		 rep+=" caractère "+ligneoffset;
		 rep+=" : "+message;
         return rep.c_str();
     }
     
    
    virtual ~ParseErreur() throw()
    {}

	unsigned int getligne();
	unsigned int getoffset();
    std::string geterreur();
private:
    std::string message;
	unsigned int ligne;
	unsigned int ligneoffset;
};

class EvalContext
{
public:
	EvalContext();
	~EvalContext();
    int lireint(std::string var);
    bool lirebool(std::string var);
    std::string lirestring(std::string var);
    void ecrireint(std::string var,int val);
    void ecrirebool(std::string var,bool val);
    void ecrirestring(std::string var,std::string val);
    void clear();
    QJsonObject getJson();
    void setFromJson(QJsonObject data);
private:
    std::map<std::string, int> intcontent;
    std::map<std::string, bool> boolcontent;
    std::map<std::string, std::string> stringcontent;
};

class Miniscritpt
{
public:
	Miniscritpt(void);
	~Miniscritpt(void);
    void AjouterScript(std::string librairie);//Warning throw execption on error
    bool Call(std::string function);
    bool Call(std::string function, int a1);
    bool Call(std::string function, int a1,int a2);
    bool Call(std::string function, std::string a1);
    EvalContext* getContext();
private:
	EvalContext* environnement;
};

enum LexemeType {PLUS, FOIS, MOINS, DIVISE, 
	NON, EGAL, SUPERIEUR, ET, OU, 
	CONCAT, NOMBREENCHAINE, CHAINEENNOMBRE, 
	CONDITION, BLOCOUVRIR, BLOCFERMER, 
	VARIABLE, FONCTIONOUVRE, FONCTIONFERME,
	COPIENOMBRE, COPIEBOOL, COPIECHAINE, 
	BOOLEEN, NOMBRE, CHAINE, ATTR, EXEC
};

struct Lexeme
{
	LexemeType lt;
    std::string valeur;
	unsigned int ligneoffset;
	unsigned int numligne;
};

enum MSArgument{ Nombre, Booleen, Chaine };

class MSFunction
{
public:
	MSFunction();
    MSFunction(std::string name, std::vector<MSArgument> prototype, std::vector<std::string> argsnames, std::vector<Lexeme>corps);
	void call(EvalContext* environment);
    std::vector<MSArgument> getPrototype();
    std::string getName();
	void passArg(unsigned int num, int val, EvalContext* environment);
    void passArg(unsigned int num, std::string val, EvalContext* environment);
	void passArg(unsigned int num, bool val, EvalContext* environment);
private:
	int evalnumber(EvalContext* environment);
    std::string evalchaine(EvalContext* environment);
	bool evalbool(EvalContext* environment);
	void eval(EvalContext* environment);
	void evalfun(EvalContext* environment);
    std::vector<MSArgument> Prototype;
    std::vector<Lexeme>Corps;
    std::vector<std::string> ArgumentName;
    std::string Name;
	unsigned int vali;
	unsigned int valt;
};

class MiniscriptParser
{
private:
    void parseme(std::string entree);
    void checkvalidity();//Warning: this throw an exception if not valid
	void checkvaliditynumber();
	void checkvaliditychaine();
	void checkvaliditybool();
	void checkvalidityexec();
	void checkvalidityfonction();
	void checkvalidityfonctioncall();
    std::vector<Lexeme> corps;
	unsigned int vali;
	unsigned int valt;
public:
    void parse(std::string entree);
};

class functionsLibrary
{
public:
	functionsLibrary();
    std::vector<MSArgument> getPrototype(std::string name);
    bool exist(std::string name);
	void add(MSFunction f);
    MSFunction* get(std::string name);
    bool isstd(std::string name);
    std::vector<MSArgument> getStdPrototype(std::string name);
    void stdcall(std::string name, EvalContext* environment);
	void passStdArg(unsigned int num, int val, EvalContext* environment);
    void passStdArg(unsigned int num, std::string val, EvalContext* environment);
	void passStdArg(unsigned int num, bool val, EvalContext* environment);
    void clearCurrent();
private:
    std::map<std::string, MSFunction> content;
    std::map<std::string, std::vector<MSArgument>> stdlib;
    std::map<std::string, void(*)(EvalContext*)> stdfuncs;
};

functionsLibrary* getLibrary();
Miniscritpt* getMS();

}

#endif
