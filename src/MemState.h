//Projet Atlantide
//    Copyright (C) 2016  Raphaël Charrondière
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#ifndef MEM_STATE_H
#define MEM_STATE_H

#include <Fact.h>
#include <string>
#include <set>
#include <map>
#include <vector>
#include <QString>

#include "Miniscritpt.h"

namespace StoriesAreMyHistory{

class MemState{
public:
    std::string getCurrentFragment();
    void setCurrentFragment(std::string f);
	void addFact( Fact fact );
	void addFacts( std::vector< Fact > fa );
	bool operator==( const MemState& m ) const;
	bool operator!=( const MemState& m ) const;
	MemState copy(  );
	bool verify( std::vector< Fact > fa );
    void Save(QString filename);
    void Load(QString filename);
    void clear();
private:
	std::set< Fact > facts;
    std::string currentFragment;
};
}

#endif
