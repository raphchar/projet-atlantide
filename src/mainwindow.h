//Projet Atlantide
//    Copyright (C) 2016  Raphaël Charrondière
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLayout>
#include <QTextEdit>
#include <QTextDocument>
#include <QTextBrowser>
#include <QFontDatabase>

#include "MemState.h"
#include "parser.h"
#include "Story.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void openAnchor(const QUrl& link);
    void contentProtect();
    void openDocument();
    void installPackage();
    void createPackage();
    void reset();
    void backToStart();
protected:
    virtual void closeEvent(QCloseEvent *e) Q_DECL_OVERRIDE;
private:
    QTextBrowser* storyView;
    void initStoryView();
    void initData();

    void resolveJump();

    void makePackage(QString packageName, QString filename, QString storyFileName, QStringList ressourcesFilenames);
    void extractPackage(QString filename);
    void openDocumentFrom(QString fileName);

    QString getLocalDir();

    //Story data
    StoriesAreMyHistory::MemState myMemory;
    StoriesAreMyHistory::Story myStory;

    QString openedData;

    void saveMemStateIfPossible();
};

#endif // MAINWINDOW_H
