//Projet Atlantide
//    Copyright (C) 2016  Raphaël Charrondière
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#ifndef PARSER_H
#define PARSER_H

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <exception>

class ParserCursor{
public:
    size_t line=1;
    size_t column=1;
    void advance(char c);
};

class ParserException: std::exception
{
public:
    ParserException(ParserCursor begin, ParserCursor end,std::string message);
    std::string message;
    ParserCursor begin;
    ParserCursor end;

    // exception interface
public:
};

class ParserNode;

class ParserCommand
{
public:
    void SetCommandName(std::string name);
    void AddArguments(ParserNode arg);
    std::string GetName();
    size_t GetNumberArguments();
    ParserNode& getArgumentRefAt(size_t position);
private:
    std::string name;
    std::vector<ParserNode> arguments;
};

class ParserNode
{
public:
    ParserNode(ParserCursor start, ParserCursor end, std::string litteral);
    ParserNode(ParserCursor start, ParserCursor end, ParserCommand command);
    ParserNode(ParserCursor start, ParserCursor end, std::vector<ParserNode> listArray);
    ParserNode();
    bool isACommand();
    bool isALitteralString();
    bool isANodeArray();
    std::string& getLitteralRef();
    ParserCommand& getCommandRef();
    ParserNode& operator[](size_t poistion);
    size_t size();
    std::vector<std::string> castLitteralAsList();
    ParserException makeException(std::string message);
    void deleteBackNodeIfLitteral();
private:
    bool isCommand=false;
    bool isLitteral=false;
    bool isArray=false;
    ParserCursor start;
    ParserCursor end;
    std::string litteral;
    std::vector<ParserNode> nodelist;
    ParserCommand command;
};

class Parser
{
public:
    Parser(std::string filename);
    ParserNode getTree();
private:
    std::ifstream is;
    ParserNode tree;

    ParserNode ParseSingle();
    ParserNode ProceedNode();
    std::string ParseCommandName();
    ParserCursor cursor;
    bool isBlankChar(char c);
    void skipBlank();
    char getnextchar();
    void rollback();
    bool cachedchar=false;
    char charcache;
    ParserCursor cursorcache;
    ParserCursor getCursor();
};

#endif // PARSER_H
