//Projet Atlantide
//    Copyright (C) 2016  Raphaël Charrondière
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>


#ifndef CHOIX_H
#define CHOIX_H

#include <string>
#include <vector>
#include "Fact.h"
#include "parser.h"
#include "contentformatter.h"
namespace StoriesAreMyHistory{
	
class ChoiceItem{
public:
	ChoiceItem(  );
    ChoiceItem( ParserCommand& cmd, ParserException ifNeeded );
    void setDescription( std::string description );
	void addFact( Fact newFact );
    std::string getDescription(  );
	std::string* getDescriptionPtr(  );
	std::vector< Fact > getLinkedFacts(  );
    std::string render( MemState& mem );
private:
	std::string description;
	std::vector< Fact > linkedFact;
    ContentFormatter formater;
};
	
class Choix{
public:
    Choix(  );
    Choix( ParserCommand &cmd, ParserException ifNeeded );
    void setDescription( std::string description );
	void addChoice( ChoiceItem newChoice );
    std::string getDescription(  );
	std::string* getDescriptionPtr(  );
	std::vector< ChoiceItem > getChoices(  );
	std::vector< ChoiceItem >::const_iterator begin(  );
	std::vector< ChoiceItem >::const_iterator end(  );
    std::string render( MemState& mem );

private:
	std::string description;
	std::vector< ChoiceItem > choices;
    ContentFormatter formater;
};
}

#endif
