//Projet Atlantide
//    Copyright (C) 2016  Raphaël Charrondière
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#ifndef FRAGMENT_H
#define FRAGMENT_H

#include <string>
#include "Choix.h"
#include "Jump.h"
#include <set>
#include "parser.h"
#include "contentformatter.h"
#include "Miniscritpt.h"

namespace StoriesAreMyHistory{


class Fragment{
public:
	Fragment( std::string name = "" );
    Fragment( std::string name, ParserNode& fromNode );
    void setContent( std::string content );
	void addFinalChoice( Choix finalChoice );
	bool hasFinalChoice(  );
	void setNextFragment( Jump nextFragment );
	void addAllInnerFactsToSet( std::set< Fact >& set);
    std::string getName(  );
	std::string getContent(  );
	std::string* getContentPtr(  );
	Choix getFinalChoice(  );
	Choix* getFinalChoicePtr(  );
	Jump getNextFragment(  );
    std::string render( MemState& mem );
    void addScript( std::string function );
    void callScript(  );
private:
    void treatTransitionCommand( ParserCommand& cmd, ParserException ifNeeded );
    void treatJumpCommand( ParserCommand& cmd, ParserException ifNeeded );
    void treatChoiceCommand( ParserCommand& cmd, ParserException ifNeeded );
	std::string name;
	std::string content;
	bool hasFinalChoiceVal;
	Choix finalChoice;
	Jump nextFragment;
    ContentFormatter formater;
    std::string script;
};	
}

#endif
