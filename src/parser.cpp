//Projet Atlantide
//    Copyright (C) 2016  Raphaël Charrondière
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#include "parser.h"

using namespace std;

Parser::Parser(std::string filename):is(filename)
{
    if(!is)
        throw string("file not aviable");
    tree=ProceedNode();
    tree.deleteBackNodeIfLitteral();
}

ParserNode Parser::getTree()
{
    return this->tree;
}

ParserNode Parser::ParseSingle()
{
    skipBlank();
    ParserCursor b=getCursor();
    char c=getnextchar();
    if(c=='/')//Command or list
    {
        string commandname=ParseCommandName();
        ParserCommand cmd;
        cmd.SetCommandName(commandname);
        skipBlank();
        c = getnextchar();
        while (c=='<') {
            ParserNode arg=ProceedNode();
            skipBlank();
            cmd.AddArguments(arg);
            c=getnextchar();
            if(c!='>')
                throw ParserException(b,getCursor(),"No closing bracket");
            skipBlank();
            c=getnextchar();
        }
        rollback();
        return ParserNode(b,getCursor(),cmd);
    }
    else if(c=='>')
    {
        rollback();
        return ParserNode(b,getCursor(),string(""));
    }
    else if (c == '<')
    {
        throw ParserException(getCursor(), getCursor(), "Missing command before bracket opening");
    }
    else //litteral or list
    {
        string res;
        bool escape=false;
        do
        {
            if(escape)
            {
                res.push_back(c);
                escape=false;
            }
            else
            {
            if(c== '|')
                escape=true;
            else
                res.push_back(c);
            }
            c=getnextchar();
        }
        while (escape || (c!='/'  && c!='<' && c!= '>' && c));
        rollback();
        return ParserNode(b,getCursor(),res);
    }
}

ParserNode Parser::ProceedNode()
{
    ParserCursor b=getCursor();
    ParserNode n = ParseSingle();
    skipBlank();
    char c=getnextchar();
    rollback();
    if(!c)
        return n;//the end
    if(c=='>')//end of node
    {
        return n;
    }
    //list
    vector<ParserNode> res;
    res.push_back(n);
    while (true) {
        ParserNode n = ParseSingle();
        res.push_back(n);
        skipBlank();
        char c=getnextchar();
        rollback();
        if(!c)
            break;//the end
        if(c=='>')//end of node
        {
            break;
        }
    }
    return ParserNode(b,getCursor(),res);
}

string Parser::ParseCommandName()
{
    char c=getnextchar();
    string res;
    while((c!='<')&&c)
    {
        if(isBlankChar(c))
             return res;
        if(c=='>'||c=='/')
            throw ParserException(getCursor(),getCursor(),"Command name can not containt any special symbol");
        res.push_back(c);
        c=getnextchar();
    }
    rollback();
    return res;
}

bool Parser::isBlankChar(char c)
{
    return (c==' '||c=='\t'||c=='\n'||c=='\r' );
}

void Parser::skipBlank()
{
    char c;
    do
    {
        c=getnextchar();
    }
    while(isBlankChar(c)&&c);
    rollback();

}

char Parser::getnextchar()
{
    if(cachedchar)
    {
        cachedchar=false;
        cursor=cursorcache;
        return charcache;
    }
    is.get(charcache);
    if(is.eof())
        charcache=0;
    cursor.advance(charcache);

    cursorcache=cursor;
    return charcache;
}

void Parser::rollback()
{
    cachedchar = true;
}

ParserCursor Parser::getCursor()
{
    if(cachedchar)
        return cursorcache;
    return cursor;
}

void ParserCursor::advance(char c)
{
    if(c=='\n')
    {
        column=1;
        line++;
    }
    else
        column++;
}

void ParserCommand::SetCommandName(string name)
{
    this->name = name;
}

void ParserCommand::AddArguments(ParserNode arg)
{
    arguments.push_back(arg);
}

string ParserCommand::GetName()
{
    return name;
}

size_t ParserCommand::GetNumberArguments()
{
    return arguments.size();
}

ParserNode &ParserCommand::getArgumentRefAt(size_t position)
{
    return arguments.at(position);
}

ParserNode::ParserNode(ParserCursor start, ParserCursor end, string litteral)
    :start(start),end(end),litteral(litteral),isLitteral(true)
{}

ParserNode::ParserNode(ParserCursor start, ParserCursor end, ParserCommand command)
:start(start),end(end),command(command),isCommand(true)
{}

ParserNode::ParserNode(ParserCursor start, ParserCursor end, std::vector<ParserNode> listArray)
    :start(start),end(end),nodelist(listArray),isArray(true)
{}

ParserNode::ParserNode()
{

}

bool ParserNode::isACommand()
{
    return isCommand;
}

bool ParserNode::isALitteralString()
{
    return isLitteral;
}

bool ParserNode::isANodeArray()
{
    return isArray;
}

string& ParserNode::getLitteralRef()
{
    return litteral;
}

ParserCommand& ParserNode::getCommandRef()
{
    return command;
}

ParserNode& ParserNode::operator [](size_t p)
{
    return nodelist.at(p);
}

size_t ParserNode::size()
{
    return nodelist.size();
}

std::vector<string> ParserNode::castLitteralAsList()
{
    size_t i,t;
    t=litteral.size();
    string item;
    vector<string> res;
    bool itemdone=false;
    for(i=0;i<t;i++)
    {
        char c=litteral[i];
        if(c==',')//separator
        {
            if(item.empty())
                throw ParserException(this->start,this->end,"found empty item in a list");
            res.push_back(item);
            item="";
            itemdone=false;
        }
        else if(c==' '||c=='\t'||c=='\r'||c=='\n')//blank at begin or end
        {
            if(!item.empty())
                itemdone=true;
        }
        else
        {
            if(itemdone)
            {
                throw ParserException(this->start,this->end,"space in a list item");
            }
            else
            {
                item.push_back(c);
            }
        }
    }
    if(!item.empty())
        res.push_back(item);
    else if (!res.empty())
        throw ParserException(this->start,this->end,"found empty item in a list");
    return res;
}

ParserException ParserNode::makeException(string message)
{
    return ParserException(start,end,message);
}

void ParserNode::deleteBackNodeIfLitteral()
{
    if(isANodeArray())
    {
        auto b=nodelist.back();
        if(b.isALitteralString())
            nodelist.pop_back();
    }
}

ParserException::ParserException(ParserCursor begin, ParserCursor end, string message)
    :begin(begin),end(end),message(message),exception()
{
}
