//Projet Atlantide
//    Copyright (C) 2016  Raphaël Charrondière
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#include "Story.h"
#include <algorithm>
#include "Miniscritpt.h"

namespace StoriesAreMyHistory{
	
Story::Story()
{
}

Story::Story(ParserNode& fromNode)
{
    if(fromNode.isALitteralString())
        throw fromNode.makeException("The story has to be a list of command");
    if(fromNode.isACommand())
        AddFromNode(fromNode.getCommandRef(),fromNode.makeException(""));
    size_t i,t;
    t=fromNode.size();
    for(i=0;i<t;i++)
    {
        if(!fromNode[i].isACommand())
            throw fromNode[i].makeException("Story must only contain command");
        AddFromNode(fromNode[i].getCommandRef(),fromNode[i].makeException(""));
    }
}
	
void Story::setTitle(std::string title, std::string subtitle)
{
	this->title = title;
	this->subtitle = subtitle;
}

void Story::setAuthor(std::string author)
{
	this->author=author;
}

void Story::setStartingFragment(std::string fragment)
{
	this->startingFragment = fragment;
}

void Story::addFragment(Fragment fragment)
{
	if(std::find(this->fragmentsNames.begin(), this->fragmentsNames.end(), fragment.getName()) != this->fragmentsNames.end())
		throw std::out_of_range("Fragment already exists");
	fragments[fragment.getName()]=fragment;
	this->fragmentsNames.push_back(fragment.getName());
}

void Story::removeFragment(std::string fragment)
{
	auto fnp = std::find(this->fragmentsNames.begin(), this->fragmentsNames.end(), fragment);
	if(fnp == this->fragmentsNames.end())
		throw std::out_of_range("Fragment does not exists");
	this->fragmentsNames.erase(fnp);
	fragments.erase(fragment);
}

std::vector< std::string > Story::getFragmentsList()
{
	return this->fragmentsNames;
}

Fragment& Story::getFragment(std::string fragment)
{
	auto fnp = std::find(this->fragmentsNames.begin(), this->fragmentsNames.end(), fragment);
	if(fnp == this->fragmentsNames.end())
		throw std::out_of_range("Fragment does not exists");
	return this->fragments[fragment];
}

std::string Story::getTitle()
{
	return this->title;
}

std::string Story::getSubtitle()
{
	return this->subtitle;
}

std::string Story::getAuthor()
{
	return this->author;
}

std::string Story::getStartingFragment()
{
    if(startingFragment!="")
        return this->startingFragment;
    else
        return this->fragmentsNames[0];
}

void Story::AddFromNode(ParserCommand &cmd, ParserException ifNeeded)
{
    if(cmd.GetName()=="Titre")
    {
        if(cmd.GetNumberArguments()!=1)
        {
            ifNeeded.message="Titre need exactly one argument";
            throw ifNeeded;
        }
        if(!cmd.getArgumentRefAt(0).isALitteralString())
            throw cmd.getArgumentRefAt(0).makeException("Titre argument must be a string");
        setTitle(cmd.getArgumentRefAt(0).getLitteralRef(), getSubtitle());
    }
    else if(cmd.GetName()=="SousTitre")
    {
        if(cmd.GetNumberArguments()!=1)
        {
            ifNeeded.message="SousTitre need exactly one argument";
            throw ifNeeded;
        }
        if(!cmd.getArgumentRefAt(0).isALitteralString())
            throw cmd.getArgumentRefAt(0).makeException("SousTitre argument must be a string");
        setTitle(getTitle(), cmd.getArgumentRefAt(0).getLitteralRef());
    }
    else if(cmd.GetName()=="Auteur")
    {
        if(cmd.GetNumberArguments()!=1)
        {
            ifNeeded.message="Auteur need exactly one argument";
            throw ifNeeded;
        }
        if(!cmd.getArgumentRefAt(0).isALitteralString())
            throw cmd.getArgumentRefAt(0).makeException("Auteur argument must be a string");
        setAuthor(cmd.getArgumentRefAt(0).getLitteralRef());
    }
    else if(cmd.GetName()=="Miniscript")
    {
        if(cmd.GetNumberArguments()!=1)
        {
            ifNeeded.message="Miniscript need exactly one argument";
            throw ifNeeded;
        }
        if(!cmd.getArgumentRefAt(0).isALitteralString())
            throw cmd.getArgumentRefAt(0).makeException("Miniscript argument must be a string");
        MiniScript::getMS()->AjouterScript(cmd.getArgumentRefAt(0).getLitteralRef());
    }
    else if(cmd.GetName()=="Scène")
    {
        if(cmd.GetNumberArguments()!=2 && cmd.GetNumberArguments()!=3)
        {
            ifNeeded.message="Scène need two or tree arguments";
            throw ifNeeded;
        }
        if(!cmd.getArgumentRefAt(0).isALitteralString())
            throw cmd.getArgumentRefAt(0).makeException("Scène first argument must be a string");

        Fragment newFrag(cmd.getArgumentRefAt(0).getLitteralRef(),cmd.getArgumentRefAt(1));
        if(cmd.GetNumberArguments()==3)
        {
            if(!cmd.getArgumentRefAt(2).isALitteralString())
                throw cmd.getArgumentRefAt(2).makeException("Scène third argument must be a string");

            newFrag.addScript(cmd.getArgumentRefAt(2).getLitteralRef());
        }
        try{addFragment(newFrag);}
        catch(std::out_of_range e)
        {throw cmd.getArgumentRefAt(0).makeException("This scene already exist");}
    }
    else
    {
        ifNeeded.message="Unknown command" + cmd.GetName();
        throw ifNeeded;
    }
}

std::set< Fact > Story::getFactsList()
{
	std::set< Fact > res;
	for(auto f : this->fragments)
		f.second.addAllInnerFactsToSet(res);
	return res;
}



}
