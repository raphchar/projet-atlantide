//Projet Atlantide
//    Copyright (C) 2016  Raphaël Charrondière
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#include "Jump.h"
namespace StoriesAreMyHistory {

Jump::Jump()
{

}

	
void Jump::setDefaultJump(std::string target)
{
	this->defaultJump = target;
}

void Jump::addConditionalJump(std::string target, std::vector< Fact > conditions)
{
    this->conditionalJumps[target] = conditions;
}

void Jump::addConditionalJump(std::string target, std::vector<std::string> conditions)
{
    std::vector< Fact > res;
    for(auto f:conditions)
        res.push_back(Fact(f));
    addConditionalJump(target,res);
}

std::string Jump::getDefaultJump()
{
	 return this->defaultJump;
}


std::map< std::string, std::vector< Fact > >::const_iterator Jump::begin()
{
	return this->conditionalJumps.begin();
}

std::map< std::string, std::vector< Fact > >::const_iterator Jump::end()
{
	return this->conditionalJumps.end();
}


}
