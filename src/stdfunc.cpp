//Projet Atlantide
//    Copyright (C) 2016  Raphaël Charrondière
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#include "stdfunc.h"
#include <QFile>
#include <QInputDialog>
#include <QMessageBox>

using namespace std;

namespace MiniScript {

QJsonObject data;


void FichierAjoutChaine(EvalContext* environment)
{
    string ou = environment->lirestring("_0");
    string var = environment->lirestring("_1");
    string val = environment->lirestring("_2");
    data[ou.c_str()].toObject()["Chaine"].toObject()[var.c_str()]=QString::fromStdString( val);
}
void FichierAjoutNombre(EvalContext* environment)
{
    string ou = environment->lirestring("_0");
    string var = environment->lirestring("_1");
    int val= environment->lireint("_2");
    data[ou.c_str()].toObject()["Nombre"].toObject()[var.c_str()]=val;
}

void FichierLireChaine(EvalContext* environment)
{
    string ou = environment->lirestring("_0");
    string var = environment->lirestring("_1");
    string dest = environment->lirestring("_2");
    string val = data[ou.c_str()].toObject()["Chaine"].toObject()[var.c_str()].toString().toStdString();
    environment->ecrirestring(dest, val);
}
void FichierLireNombre(EvalContext* environment)
{
    string ou = environment->lirestring("_0");
    string var = environment->lirestring("_1");
    string dest = environment->lirestring("_2");
    int val = data[ou.c_str()].toObject()["Nombre"].toObject()[var.c_str()].toInt();
    environment->ecrireint(dest, val);
}

void FichierSauver(EvalContext* environment)
{
    string f = environment->lirestring("_0");
    QFile saveFile(QString::fromStdString(f));

    if (!saveFile.open(QIODevice::WriteOnly)) {
        qWarning("Couldn't open save file.");
        return;
    }
    else
    {
        QJsonDocument saveDoc(data);
        saveFile.write( saveDoc.toJson());
        saveFile.close();
    }
}
void FichierCharger(EvalContext* environment)
{
    string f = environment->lirestring("_0");
    QFile loadFile(QString::fromStdString(f));
    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open save file.");
        return;
    }
    else
    {
        QByteArray saveData = loadFile.readAll();
        QJsonDocument loadDoc( QJsonDocument::fromJson(saveData));
        data =loadDoc.object();
    }
    loadFile.close();
}

void DemandeInfo(EvalContext *environment)
{
    string titre = environment->lirestring("_0");
    string message = environment->lirestring("_1");
    string dest = environment->lirestring("_2");
    string transactionval = environment->lirestring("_3");
    //environment->ecrireint(dest, val);
    bool ok;
    QString text = QInputDialog::getText(nullptr, QString::fromStdString(titre),
                                         QString::fromStdString(message), QLineEdit::Normal,
                                         QString(), &ok);
    if (ok )
        environment->ecrirestring(dest,text.toStdString());
    environment->ecrirebool(transactionval,ok);
}

void Message(EvalContext *environment)
{
    string titre = environment->lirestring("_0");
    string message = environment->lirestring("_1");
    QMessageBox::information(nullptr, QString::fromStdString(titre),
                             QString::fromStdString(message),
                             QMessageBox::Close);
}

void Erreur(EvalContext *environment)
{
    string titre = environment->lirestring("_0");
    string message = environment->lirestring("_1");
    QMessageBox::critical(nullptr, QString::fromStdString(titre),
                             QString::fromStdString(message),
                             QMessageBox::Close);
}

void Avertissement(EvalContext *environment)
{
    string titre = environment->lirestring("_0");
    string message = environment->lirestring("_1");
    QMessageBox::warning(nullptr, QString::fromStdString(titre),
                             QString::fromStdString(message),
                             QMessageBox::Close);
}

void DemandeNombre(EvalContext *environment)
{
    string titre = environment->lirestring("_0");
    string message = environment->lirestring("_1");
    string dest = environment->lirestring("_2");
    string transactionval = environment->lirestring("_3");
    //environment->ecrireint(dest, val);
    bool ok;
    int res = QInputDialog::getInt(nullptr, QString::fromStdString(titre),
                                         QString::fromStdString(message),
                                        0,-2147483647,2147483647,1,&ok);
    if (ok )
        environment->ecrireint(dest,res);
    environment->ecrirebool(transactionval,ok);
}

void ValiderCode(EvalContext *environment)
{
    string code1 = environment->lirestring("_0");
    string code2 = environment->lirestring("_1");
    string valid = environment->lirestring("_2");
    string dest = environment->lirestring("_3");
    if(code1==code2)
        environment->ecrirestring(dest,valid);
}

}
