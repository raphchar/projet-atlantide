//Projet Atlantide
//    Copyright (C) 2016  Raphaël Charrondière
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#include "Fragment.h"

namespace StoriesAreMyHistory {
Fragment::Fragment(std::string name):
	hasFinalChoiceVal(false), name(name)
{
}

Fragment::Fragment(std::string name, ParserNode &fromNode):
    hasFinalChoiceVal(false),name(name)
{
    if(fromNode.isALitteralString())
    {
        formater.FormatFromLitteralString(content,fromNode.getLitteralRef());
    }
    else if(fromNode.isACommand())
    {
        if(fromNode.getCommandRef().GetName()=="Transition")
            treatTransitionCommand(fromNode.getCommandRef(),fromNode.makeException(""));
        else
            formater.FormatFromCommand(content,fromNode.getCommandRef(),fromNode.makeException(""));
    }
    else
    {
        size_t i,t;
        t=fromNode.size();
        for(i=0;i<t;i++)
        {
            if(fromNode[i].isALitteralString())
                 formater.FormatFromLitteralString(content,fromNode[i].getLitteralRef());
            else
            {
                if(fromNode[i].getCommandRef().GetName()=="Transition")
                    treatTransitionCommand(fromNode[i].getCommandRef(),fromNode[i].makeException(""));
                else
                    formater.FormatFromCommand(content,fromNode[i].getCommandRef(),fromNode[i].makeException(""));
            }
        }
    }
}

void Fragment::setContent(std::string content)
{
	this->content = content;
}

void Fragment::addFinalChoice(Choix finalChoice)
{
	this->finalChoice = finalChoice;
	hasFinalChoiceVal = true;
}

bool Fragment::hasFinalChoice()
{
	return hasFinalChoiceVal;
}

void Fragment::setNextFragment(Jump nextFragment)
{
	this->nextFragment = nextFragment;
}

std::string Fragment::getName()
{
	return this->name;
}

std::string Fragment::getContent()
{
	return this->content;
}

std::string* Fragment::getContentPtr()
{
	return &this->content;
}

Choix Fragment::getFinalChoice()
{
	return this->finalChoice;
}

Choix* Fragment::getFinalChoicePtr()
{
	return &this->finalChoice;
}

Jump Fragment::getNextFragment()
{
    return this->nextFragment;
}

std::string Fragment::render(MemState &mem)
{
    std::string res=content;
    formater.FinalFormat(res,mem);
    formater.FormatAsContent(res);
    if(hasFinalChoice())
    {
        res+=finalChoice.render(mem);
    }
    else
        res+=formater.FormatAsNoChoice();
    return res;
}

void Fragment::addScript(std::string function)
{
    script=function;
}

void Fragment::callScript()
{
    if(!script.empty())
        MiniScript::getMS()->Call(script);
}

void Fragment::treatTransitionCommand(ParserCommand &cmd, ParserException ifNeeded)
{
    if(cmd.GetNumberArguments()!=1)
    {
        ifNeeded.message="Transition command has exactly one argument";
        throw ifNeeded;
    }
    ParserNode& fromNode=cmd.getArgumentRefAt(0);
    if(fromNode.isALitteralString())
    {
        throw fromNode.makeException("Transition argument is not a litteral");
    }
    else if(fromNode.isACommand())
    {
        if(fromNode.getCommandRef().GetName()=="Suite")
            treatJumpCommand(fromNode.getCommandRef(),fromNode.makeException(""));
        else
            throw fromNode.makeException("No mean for this command, it should be Suite");
    }
    else
    {
        size_t i,t;
        t=fromNode.size();
        for(i=0;i<t;i++)
        {
            if(fromNode[i].isALitteralString())
                 throw fromNode.makeException("Transition argument does not contain string");
            else
            {
                if(fromNode[i].getCommandRef().GetName()=="Suite")
                    treatJumpCommand(fromNode[i].getCommandRef(),fromNode[i].makeException(""));
                else if(fromNode[i].getCommandRef().GetName()=="Choix")
                    treatChoiceCommand(fromNode[i].getCommandRef(),fromNode[i].makeException(""));
                else
                    throw fromNode[i].makeException("Not known feature");
            }
        }
    }
}

void Fragment::treatJumpCommand(ParserCommand &cmd, ParserException ifNeeded)
{
    if(cmd.GetNumberArguments()!=1)
    {
        ifNeeded.message="Suite command has exactly one argument";
        throw ifNeeded;
    }
    ParserNode& fromNode=cmd.getArgumentRefAt(0);

    if(fromNode.isALitteralString())
    {
        throw fromNode.makeException("Suite argument is not a litteral");
    }
    else if(fromNode.isACommand())
    {
        ParserCommand& p=fromNode.getCommandRef();
        if(p.GetName()=="Porte")
        {
            if(p.GetNumberArguments()==1)
            {
                if(!p.getArgumentRefAt(0).isALitteralString())
                    throw fromNode.makeException("Argument shoud be a string");
                this->nextFragment.setDefaultJump(p.getArgumentRefAt(0).getLitteralRef());
            }
            else if(p.GetNumberArguments()==2)
            {
                if(!p.getArgumentRefAt(0).isALitteralString())
                    throw fromNode.makeException("Argument 1 shoud be a list");
                if(!p.getArgumentRefAt(1).isALitteralString())
                    throw fromNode.makeException("Argument 2 shoud be a string");
                this->nextFragment.addConditionalJump(
                            p.getArgumentRefAt(1).getLitteralRef(),
                            p.getArgumentRefAt(0).castLitteralAsList());
            }
            else {
                throw fromNode.makeException("Number of arguments should be one or two");
            }
        }
        else
            throw fromNode.makeException("No mean for this command, it should be Porte");
    }
    else
    {
        size_t i,t;
        t=fromNode.size();
        for(i=0;i<t;i++)
        {
            if(fromNode[i].isALitteralString())
                throw fromNode.makeException("Suite argument does not contain string");
            else
            {
                if(fromNode[i].getCommandRef().GetName()=="Porte")
                {
                    ParserCommand& p=fromNode[i].getCommandRef();
                    if(p.GetNumberArguments()==1)
                    {
                        if(!p.getArgumentRefAt(0).isALitteralString())
                            throw fromNode.makeException("Argument shoud be a string");
                        this->nextFragment.setDefaultJump(p.getArgumentRefAt(0).getLitteralRef());
                    }
                    else if(p.GetNumberArguments()==2)
                    {
                        if(!p.getArgumentRefAt(0).isALitteralString())
                            throw fromNode.makeException("Argument 1 shoud be a list");
                        if(!p.getArgumentRefAt(1).isALitteralString())
                            throw fromNode.makeException("Argument shoud be a string");
                        this->nextFragment.addConditionalJump(
                                    p.getArgumentRefAt(1).getLitteralRef(),
                                    p.getArgumentRefAt(0).castLitteralAsList());
                    }
                    else {
                        throw fromNode.makeException("Number of arguments should be one or two");
                    }
                }
                else
                    throw fromNode[i].makeException("Not known feature");
            }
        }
    }
}

void Fragment::treatChoiceCommand(ParserCommand &cmd, ParserException ifNeeded)
{
    hasFinalChoiceVal=true;
    finalChoice = Choix(cmd,ifNeeded);
}

void Fragment::addAllInnerFactsToSet(std::set< Fact >& set)
{
	auto choices = this->finalChoice.getChoices();
	for(ChoiceItem choice : choices)
	{
		auto choiceFacts = choice.getLinkedFacts();
		for( Fact f : choiceFacts)
			set.insert(f);
	}
	
	for(auto cJump: this->nextFragment)
	{
		for(auto f:cJump.second)
			set.insert(f);
    }
}





}
