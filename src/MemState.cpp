//Projet Atlantide
//    Copyright (C) 2016  Raphaël Charrondière
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#include "MemState.h"
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

namespace StoriesAreMyHistory {

std::string MemState::getCurrentFragment()
{
    return currentFragment;
}

void MemState::setCurrentFragment(std::string f)
{
    if(f!="")
    {
        if(f.back()=='&')
        {
            f.pop_back();
            currentFragment = MiniScript::getMS()->getContext()->lirestring(f);
            return;
        }
    }
    currentFragment = f;
}

void MemState::addFact(Fact fact)
{
	this->facts.insert(fact);
}

bool MemState::operator!=(const MemState& m) const
{
	return this->facts != m.facts;
}

bool MemState::operator==(const MemState& m) const
{
	return this->facts == m.facts;
}

MemState MemState::copy()
{
	MemState res;
	res.facts = this->facts;
	return res;
}

bool MemState::verify(std::vector< Fact > fa)
{
	for(auto f:fa)
	{
		if(facts.find(f) == facts.end())
			return false;
	}
    return true;
}

void MemState::Save(QString filename)
{
    QFile saveFile(filename);

    if (!saveFile.open(QIODevice::WriteOnly)) {
        qWarning("Couldn't open save file.");
        return;
    }
    QJsonObject data;
    data["Scène"]=QString::fromStdString(currentFragment);
    QJsonArray datafacts;
    for(Fact f:facts)
        datafacts.push_back(QString::fromStdString(f.getName()));
    data["Faits"]=datafacts;

    MiniScript::EvalContext* context = MiniScript::getMS()->getContext();
    data["Variables"]=context->getJson();
    QJsonDocument saveDoc(data);
    saveFile.write( saveDoc.toJson());
    saveFile.close();
}

void MemState::Load(QString filename)
{
    QFile loadFile(filename);
    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open save file.");
        return;
    }
    QByteArray saveData = loadFile.readAll();
    QJsonDocument loadDoc( QJsonDocument::fromJson(saveData));
    QJsonObject data =loadDoc.object();
    loadFile.close();
    currentFragment=data["Scène"].toString().toStdString();
    QJsonArray f=data["Faits"].toArray();
    facts.clear();
    for(auto fact:f)
        facts.insert(Fact(fact.toString().toStdString()));
    MiniScript::getMS()->getContext()->setFromJson(data["Variables"].toObject());
}

void MemState::clear()
{
    currentFragment="";
    facts.clear();
    MiniScript::getMS()->getContext()->clear();
}

void MemState::addFacts(std::vector< Fact > fa)
{
	for(auto f:fa)
		this->facts.insert(f);
}


	
}
