//Projet Atlantide
//    Copyright (C) 2016  Raphaël Charrondière
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#ifndef STORY_H
#define STORY_H

#include <string>
#include <vector>
#include "Fragment.h"
#include <map>
#include <set>
#include "parser.h"
#include "MemState.h"


namespace StoriesAreMyHistory{

class Story{
public:
	Story(  );
    Story( ParserNode& fromNode );
    void setTitle( std::string title, std::string subtitle = "" );
	void setAuthor( std::string author );
	void addFragment( Fragment fragment );
	void removeFragment( std::string fragment );
	void setStartingFragment( std::string fragment );
	std::vector< std::string > getFragmentsList(  );
	std::set< Fact > getFactsList(  );
	Fragment& getFragment( std::string fragment );
	std::string getTitle(  );
	std::string getSubtitle(  );
	std::string getAuthor(  );
    std::string getStartingFragment(  );
private:
    void AddFromNode( ParserCommand& cmd, ParserException ifNeeded );
	std::string author;
	std::string title;
	std::string subtitle;
	std::string startingFragment;
	std::vector< std::string > fragmentsNames;
	std::map< std::string, Fragment > fragments;
};
}

#endif
