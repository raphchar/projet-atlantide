//Projet Atlantide
//    Copyright (C) 2016  Raphaël Charrondière
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#include "mainwindow.h"
#include <QPushButton>
#include <QApplication>
#include <QFile>
#include <QTextCodec>
#include <QMessageBox>
#include <QFileDialog>
#include <QMenuBar>
#include <QMenu>
#include <QDir>
#include <QInputDialog>
#include <QStandardPaths>
#include <QDataStream>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QCloseEvent>
#include <QDirIterator>

#include <contentformatter.h>
#include "Miniscritpt.h"

using namespace std;


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    setWindowTitle(QCoreApplication::applicationName());
    initStoryView();
    initData();
    QMenu *menu = menuBar()->addMenu(tr("&File"));

    //const QIcon newIcon = QIcon::fromTheme("document-new", QIcon(rsrcPath + "/filenew.png"));
    /*QAction *a = */menu->addAction(  tr("&Ouvrir"), this, &MainWindow::openDocument);
    menu->addAction(  tr("&Installer un nouveau packet"), this, &MainWindow::installPackage);
    menu->addAction(  tr("&Creer un nouveau packet"), this, &MainWindow::createPackage);
    menu->addAction(  tr("&Oublier ce qu'il s'est passé"), this, &MainWindow::reset);
    menu->addAction(  tr("&Retour accueil"), this, &MainWindow::backToStart);

    size().width();
}

MainWindow::~MainWindow()
{

}

void MainWindow::openAnchor(const QUrl &link)
{
    std::string action = link.url(QUrl::None).toStdString();

    if(!action.empty())
    {
        if(action[0]=='+')
        {
            QString file;
            file=getLocalDir()+"/"+QString::fromStdString(action).remove(0,1)+"/story.txt";
            openDocumentFrom(file);
            return;
        }
    }

    std::string frag = myMemory.getCurrentFragment();
    if(action!="start")
        if(frag!="")
            myStory.getFragment(frag).callScript();

    if(action=="start")
    {
        myMemory.setCurrentFragment(myStory.getStartingFragment());
        std::string out=myStory.getFragment(myMemory.getCurrentFragment()).render(myMemory);
        StoriesAreMyHistory::ContentFormatter::ApplyRender(out,size().width());
        storyView->setHtml(QString::fromStdString(out));
        return;
    }
    else if(action == "next")
    {
        resolveJump();
    }
    else
    {
        StoriesAreMyHistory::Fragment& f=myStory.getFragment(myMemory.getCurrentFragment());
        int choix = QString::fromStdString(action).toInt();
        StoriesAreMyHistory::ChoiceItem ci = f.getFinalChoicePtr()->getChoices()[choix];
        myMemory.addFacts(ci.getLinkedFacts());
        resolveJump();
    }
}

void MainWindow::resolveJump()
{
    try
    {
    StoriesAreMyHistory::Fragment& f=myStory.getFragment(myMemory.getCurrentFragment());
    for(auto c:f.getNextFragment())
    {
        if(myMemory.verify(c.second))
        {
            myMemory.setCurrentFragment(c.first);
            std::string out=myStory.getFragment(myMemory.getCurrentFragment()).render(myMemory);
            StoriesAreMyHistory::ContentFormatter::ApplyRender(out,size().width());
            storyView->setHtml(QString::fromStdString(out));
            return;
        }
    }
    myMemory.setCurrentFragment(f.getNextFragment().getDefaultJump());
    std::string out=myStory.getFragment(myMemory.getCurrentFragment()).render(myMemory);
    StoriesAreMyHistory::ContentFormatter::ApplyRender(out,size().width());
    storyView->setHtml(QString::fromStdString(out));
    return;
    }
    catch(...)
    {
        std::string endPage=StoriesAreMyHistory::ContentFormatter::EndPage(
                    myStory.getTitle(),
                    myStory.getSubtitle(),
                    myStory.getAuthor());
        StoriesAreMyHistory::ContentFormatter::ApplyRender(endPage,size().width());
        storyView->setHtml(QString::fromStdString(endPage));
    }
}

#define PACKAGE_MAGIC_NUMBER (int)1 //increment when Qt version or package norm changes

void MainWindow::makePackage(QString packageName, QString filename, QString storyFileName, QStringList ressourcesFilenames)
{
    QFile file(filename);
    QString wd=QFileInfo(filename).absoluteDir().absolutePath();
    file.open(QIODevice::WriteOnly);
    QDir::setCurrent(wd);

    QDataStream out(&file);   // we will serialize the data into the file
    out.setVersion(QDataStream::Qt_5_6);
    out<< PACKAGE_MAGIC_NUMBER;
    out<< packageName;
    out<<ressourcesFilenames.size();
    //test
    {
        QFile storyfile(storyFileName);
        storyfile.open(QFile::ReadOnly);
        QByteArray array = storyfile.readAll();
        storyfile.close();
        QByteArray res = qCompress(array,9);
        out << QString("story.txt");
        out<<res;   // serialize a string
    }
    for(QString f:ressourcesFilenames)
    {
        QFile resfile(f);
        resfile.open(QFile::ReadOnly);
        QByteArray array = resfile.readAll();
        resfile.close();
        QByteArray res = qCompress(array,9);
        out << QString(f);
        out << res;   // serialize a string
    }
    file.close();

}

void MainWindow::extractPackage(QString filename)
{
    QFile file(filename);
     file.open(QIODevice::ReadOnly);
     QDataStream in(&file);    // read the data serialized from the file
     QString packageName;
     int versionCheck;
     int nbres;
     in>>versionCheck;
     if(versionCheck!= PACKAGE_MAGIC_NUMBER)
     {
         QMessageBox::critical(this, QCoreApplication::applicationName(),
                                  "Le package sélectionné est trop récent, veuillez mettre à jour le logiciel pour en profiter",
                                  QMessageBox::Close);
         return;
     }
     in>>packageName>>nbres;
     nbres++;
     QString edir=getLocalDir()+"/"+packageName+"/";
     QDir().mkdir(edir);
     for(int i=0;i<nbres;i++)
     {
         QString filename;
       QByteArray res;
       in >> filename >> res;
       QByteArray array = qUncompress(res);
       QFile resfile(edir+filename);
       resfile.open(QFile::WriteOnly);
       resfile.write(array);
       resfile.close();
     }
}

void MainWindow::openDocumentFrom(QString fileName)
{
    saveMemStateIfPossible();
    openedData="";
    QString wd=QFileInfo(fileName).absoluteDir().absolutePath();
    QDir::setCurrent(wd);
    try{
        MiniScript::getLibrary()->clearCurrent();
        Parser p(fileName.toStdString());
        ParserNode tree = p.getTree();
        myStory = StoriesAreMyHistory::Story(tree);
        myMemory.clear();
        std::string titlepage=StoriesAreMyHistory::ContentFormatter::TitlePage(
                    myStory.getTitle(),
                    myStory.getSubtitle(),
                    myStory.getAuthor());
        StoriesAreMyHistory::ContentFormatter::ApplyRender(titlepage,size().width());
        storyView->setHtml(
                    QString::fromStdString(titlepage
                        ));

        if(QFile::exists(fileName+".mem"))
        {
            myMemory.Load(fileName+".mem");
            if(myMemory.getCurrentFragment()!="")
            {
                std::string out=myStory.getFragment(myMemory.getCurrentFragment()).render(myMemory);
                StoriesAreMyHistory::ContentFormatter::ApplyRender(out,size().width());
                storyView->setHtml(QString::fromStdString(out));
            }
        }
        openedData=fileName;
    }
    catch(ParserException e)
    {
        QString m = QString(e.message.c_str())+"\nat"+QString::number(e.begin.line)+","+QString::number(e.begin.column);
        QMessageBox::information(this, QCoreApplication::applicationName(),
                                 m,
                                 QMessageBox::Close);
    }
    catch(std::string e)
    {
        QString m = QString::fromStdString(e);
        QMessageBox::information(this, QCoreApplication::applicationName(),
                                 m,
                                 QMessageBox::Close);
    }
    catch(MiniScript::ParseErreur e)
    {
        QString m = QString(e.geterreur().c_str())+"\nat"+QString::number(e.getligne())+","+QString::number(e.getoffset());
        QMessageBox::information(this, QCoreApplication::applicationName(),
                                 m,
                                 QMessageBox::Close);
    }
}

QString MainWindow::getLocalDir()
{
    return QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation)+"/ProjectAtlantide";
}

void MainWindow::saveMemStateIfPossible()
{
    if(myMemory.getCurrentFragment()=="")
        return;
    if(openedData=="")
        return;
    myMemory.Save(openedData+".mem");
}

void MainWindow::initStoryView()
{
    storyView = new QTextBrowser(this);
    setCentralWidget(storyView);
    storyView->setOpenLinks(false);

    connect(storyView,SIGNAL(anchorClicked(QUrl)),this,SLOT(openAnchor(QUrl)));
    connect(storyView,SIGNAL(selectionChanged()), this, SLOT(contentProtect()));


    QLatin1String f(":/longpage.html");
    QFile file(f);
    if (file.open(QFile::ReadOnly))
    {
        QByteArray data = file.readAll();
        QTextCodec *codec = Qt::codecForHtml(data);
        QString str = codec->toUnicode(data);
        storyView->setHtml(str);
    }
    else
        storyView->setHtml("<h1>Coucou</h1><p>Tu vas bien ?<a href=\"oui\">Oui</a></p>");
    storyView->zoomIn(20);
}

void MainWindow::initData()
{
    QDir fontDir(":/fonts");
    QStringList filters;
         filters << "*.ttf";
    QStringList fonts = fontDir.entryList(filters,QDir::Files);

    for(auto f:fonts)
    {
        QFontDatabase::addApplicationFont(":/fonts/"+f);
    }
    /*const QString localDir=QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation)+"/ProjectAtlantide";
    QDir().mkdir(localDir);
    QString target=localDir+"/story.txt";
    QFile file(":/story.txt");
    if (file.open(QFile::ReadOnly))
    {
        QByteArray data = file.readAll();

        QFile file(target);
        file.open(QFile::ReadWrite);
        file.write(data);
        file.close();

    }

    try{
        Parser p(target.toStdString());
        ParserNode tree = p.getTree();
        myStory = StoriesAreMyHistory::Story(tree);

        std::string titlepage=StoriesAreMyHistory::ContentFormatter::TitlePage(
                    myStory.getTitle(),
                    myStory.getSubtitle(),
                    myStory.getAuthor());
        StoriesAreMyHistory::ContentFormatter::ApplyRender(titlepage,size().width());
        storyView->setHtml(
                    QString::fromStdString(titlepage
                        ));
    }
    catch(ParserException e)
    {
        QString m = QString(e.message.c_str())+"\nat"+QString::number(e.begin.line)+","+QString::number(e.begin.column);
        QMessageBox::information(this, QCoreApplication::applicationName(),
                                 m,
                                 QMessageBox::Close);
    }
    catch(MiniScript::ParseErreur e)
    {
        QString m = QString(e.geterreur().c_str())+"\nat"+QString::number(e.getligne())+","+QString::number(e.getoffset());
        QMessageBox::information(this, QCoreApplication::applicationName(),
                                 m,
                                 QMessageBox::Close);
    }
    catch(std::string e)
    {
        QString m = QString::fromStdString(e);
        QMessageBox::information(this, QCoreApplication::applicationName(),
                                 m,
                                 QMessageBox::Close);
    }*/

    /*QStringList list;
    list.push_back("cute.jpg");
    makePackage("test","test/test.atlantidepack","story.txt",list);*/
    backToStart();
}

void MainWindow::contentProtect()
{
    auto cur = storyView->textCursor();
    cur.clearSelection();
    storyView->setTextCursor(cur);
}

void MainWindow::openDocument()
{
    QString localDir=getLocalDir();
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                     localDir,
                                                      tr("All (*)"));
    if(fileName=="")
        return;
    openDocumentFrom(fileName);

}

void MainWindow::installPackage()
{
    QString localDir=getLocalDir();
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                     localDir,
                                                      tr("Packet pour Projet Atlantide (*.atlantidepack)"));
    if(fileName == "")
        return;
    extractPackage(fileName);
}

void MainWindow::createPackage()
{
    QString localDir=getLocalDir();
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                     localDir,
                                                      tr("Preparation (*.json)"));
    if(fileName == "")
        return;

    QFile loadFile(fileName);
    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open save file.");
        return;
    }
    QByteArray saveData = loadFile.readAll();
    QJsonDocument loadDoc( QJsonDocument::fromJson(saveData));
    QJsonObject data =loadDoc.object();
    QString name=data["Aventure"].toString();
    QString mainFile=data["Fichier"].toString();
    QJsonArray res=data["Ressources"].toArray();
    QStringList ressources;
    for(QJsonValueRef o: res)
        ressources.push_back(o.toString());
    QString wd=QFileInfo(fileName).absoluteDir().absolutePath();
    QDir::setCurrent(wd);
    this->makePackage(name,name+".atlantidepack",mainFile,ressources);
}

void MainWindow::reset()
{
    myMemory.clear();
    if(openedData!="")
    {
        if(QFile().exists(openedData+".mem"))
        {
            QFile(openedData+".mem").remove();
        }
        openDocumentFrom(openedData);
    }
}

void MainWindow::backToStart()
{
    saveMemStateIfPossible();
    QStringList files;
    QDir d(getLocalDir());
    QStringList dirs=d.entryList(QDir::Dirs);
    //QDirIterator it(d);
    for(QString e: dirs) {

          if((e!=".") && (e!=".."))
          {
              if(QFile::exists(getLocalDir()+"/"+e+"/story.txt"))
                  files.push_back(e);
          }
      }
    std::string accueil=StoriesAreMyHistory::ContentFormatter::Accueil(files);
    StoriesAreMyHistory::ContentFormatter::ApplyRender(accueil,size().width());
    storyView->setHtml(QString::fromStdString(accueil));
}

void MainWindow::closeEvent(QCloseEvent *e)
{
    saveMemStateIfPossible();
    e->accept();
}
